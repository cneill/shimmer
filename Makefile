# Makefile for shimmer
# You must have the Go compiler and tools installed to build this software.

VERS=$(shell sed <shimmer.go -n -e '/version string/s/.*"\([^"]*\)"/\1/p')

shimmer: shimmer.go
	go build

clean:
	go clean
	rm -f *.html *.1

install: shimmer
	go install

get:
	go get -u ./...	# go get -u=patch for patch releases

# Three-phase testing sequence
# 1. Validate the test code (skipped if shellcheck is not installed)
# 2. Run tests on the primitives used by the Shimmer storage layer
# 3. Run unit tests for the primitives inside shimmer
# 4. Run end-to-end tests on shimmer
# If any of these stages fail we need to bail out on later ones.
check: shimmer
	@-shellcheck -f gcc gittest shimmertest
	@golint -set_exit_status ./...
	@./gittest | ./tapview
	@go test ./...
	@./shimmertest | ./tapview
	@pylint --score=n dazzle

fmt:
	gofmt -w .

gofmt:
	gofmt -s -d shimmer.go | patch -p0

fixme:
	@if command -v rg; then \
		rg --no-heading FIX''ME; \
	else \
		find . -type f -exec grep -n FIX''ME {} /dev/null \; | grep -v "[.]git"; \
	fi

SOURCES = README.adoc INSTALL.adoc COPYING NEWS.adoc control shimmer.go \
		shimmer.adoc shimmer-guide.adoc prospectus.adoc data-design.adoc shimmertest \
		Makefile

.SUFFIXES: .html .adoc .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	asciidoc $<

version:
	@echo $(VERS)

shimmer-$(VERS).tar.gz: $(SOURCES) shimmer.1
	mkdir shimmer-$(VERS)
	cp $(SOURCES) shimmer-$(VERS)
	tar -czf shimmer-$(VERS).tar.gz shimmer-$(VERS)
	rm -fr shimmer-$(VERS)
	ls -l shimmer-$(VERS).tar.gz

release: shimmer-$(VERS).tar.gz shimmer.html hacking.html
	shipper version=$(VERS) | sh -e -x

refresh: shimmer.html
	shipper -N -w version=$(VERS) | sh -e -x
