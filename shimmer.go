// Copyright by Eric S. Raymond
// SPDX-License-Identifier: BSD-2-Clause
package main

import (
	"bufio"
	"bytes"
	//"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"net/http"
	"net/mail"
	"net/textproto"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	fqdn "github.com/Showmax/go-fqdn"
	//minisign "github.com/aead/minisign"
	shlex "github.com/anmitsu/go-shlex"
	asciidoc "github.com/bytesparadise/libasciidoc"
	mconfig "github.com/bytesparadise/libasciidoc/pkg/configuration"
	tcell "github.com/gdamore/tcell/v2"
	markdown "github.com/gomarkdown/markdown"
	mhtml "github.com/gomarkdown/markdown/html"
	term "golang.org/x/term"
	yaml "gopkg.in/yaml.v3"
)

// Change these in the unlikely the event this is ported to Windows
const userReadWriteMode = 0644       // rw-r--r--
const userReadWriteSearchMode = 0775 // rwxrwxr-x

var verbose uint
var hostname string
var clockbase int64
var repodir string
var nomail bool

// rfc3339 makes a UTC RFC3339 string with milliseconds from a system timestamp.
func rfc3339(t time.Time) string {
	return t.UTC().Format("2006-01-02T15:04:05.000Z")
}

const (
	logSHOUT    uint = 1 << iota // Errors and urgent messages
	logWARN                      // Exceptional condition, probably not bug (default log level)
	logCOMMANDS                  // Show commands as they are executed
)

// logEnable is a hook to set up log-message filtering.
func logEnable(level uint) bool {
	return verbose >= level
}

func speak(msg string, args ...interface{}) {
	fmt.Printf("shimmer: "+msg+"\n", args...)
}

func croak(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "shimmer: "+msg+"\n", args...)
	os.Exit(1)
}

/* Bottom layer: manage slave processes */

// readFromProcess - runs  commamd, returns stream we can read its output from.
func readFromProcess(command string) (io.ReadCloser, *exec.Cmd, error) {
	fields, err := shlex.Split(command, true)
	if err != nil {
		return nil, nil, fmt.Errorf("splitting %q: %s", command, err)
	}
	cmd := exec.Command(fields[0], fields[1:]...)
	cmd.Stdin = os.Stdin
	stdout, err := cmd.StdoutPipe()
	cmd.Stderr = cmd.Stdout
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		speak("%s: reading from '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

// lineByLine - feed each line of the output from a command to a hook.
func lineByLine(command string, errfmt string, hook func(string) error) error {
	stdout, cmd, err1 := readFromProcess(command)
	if err1 != nil {
		return err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return fmt.Errorf(errfmt, err2)
		}
		// Ugh, wecan only pass out the last error
		err1 = hook(line)
	}
	return err1
}

// writeToProcess - return a stream we can write to a command with.
func writeToProcess(command string) (io.WriteCloser, *exec.Cmd, error) {
	fields, err := shlex.Split(command, true)
	if err != nil {
		return nil, nil, fmt.Errorf("splitting %q: %s", command, err)
	}
	cmd := exec.Command(fields[0], fields[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		speak("%s: writing to '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

// captureFromProcess - runs a specified command, capturing the output.
func captureFromProcess(command string) (string, error) {
	if logEnable(logCOMMANDS) {
		speak("%s: capturing %s", rfc3339(time.Now()), command)
	}
	fields, err := shlex.Split(command, true)
	if err != nil {
		return "", fmt.Errorf("splitting %q: %s", command, err)
	}
	cmd := exec.Command(fields[0], fields[1:]...)
	content, err := cmd.CombinedOutput()
	if logEnable(logCOMMANDS) {
		speak(string(content))
	}
	return string(content), err
}

/* Middle layer: communicate with the current repository */

// VCS encapsulates generation commands to the underlying VCS
type VCS struct {
	user      mail.Address
	testclock time.Time
}

func exists(pathname string) bool {
	_, err := os.Stat(pathname)
	return !os.IsNotExist(err)
}

func isdir(pathname string) bool {
	st, err := os.Stat(pathname)
	return err == nil && st.Mode().IsDir()
}

// newVCS - return a control
func newVCS() *VCS {
	var out VCS

	out.user.Name = "shimmer"
	out.user.Address = hostname

	out1, err1 := exec.Command("git", "config", "user.name").CombinedOutput()
	out2, err2 := exec.Command("git", "config", "user.email").CombinedOutput()
	if err1 == nil && len(out1) != 0 && err2 == nil && len(out2) != 0 {
		out.user.Name = strings.Trim(string(out1), "\n")
		out.user.Address = strings.Trim(string(out2), "\n")
	}

	if clockbase > 0 {
		out.testclock = out.testclock.Add(time.Second * time.Duration(clockbase))
	}

	return &out
}

func (vcs VCS) isRepository(dir string) bool {
	return exists(filepath.Join(dir, ".git")) && isdir(filepath.Join(dir, ".git"))
}

func (vcs VCS) repoDirectory() string {
	return ".git"
}

func (vcs VCS) currentBranch() (string, error) {
	// Could be git rev-parse --abbrev-ref HEAD, that would work in  Gut versions
	// befire 2.22, but it will return an un-useful thing in detached-head state
	s, e := captureFromProcess("git branch --show-current")
	s = strings.TrimSpace(s)
	return s, e
}

func (vcs VCS) configBeforeWrite() {
	// Before we write any notes, we want to make sure they will
	// be peserved through amend and rebase..
	captureFromProcess("git config notes.rewriteRef refs/notes/shimmer")
	captureFromProcess("git config notes.rewrite.amend true")
	captureFromProcess("git config notes.rewrite.rebase true")
}

func (vcs *VCS) time() time.Time {
	if !vcs.testclock.IsZero() {
		return vcs.testclock
	}
	return time.Now()
}

func (vcs *VCS) userid() string {
	return strings.Replace(vcs.user.String(), `"`, ``, -1)
}

func (vcs *VCS) hasBranches() bool {
	stdout, _, err := readFromProcess("git branch")
	if err != nil {
		return false
	}
	b, err := ioutil.ReadAll(stdout)
	return err == nil && len(b) > 0
}

func (vcs *VCS) getCommitters() orderedStringSet {
	var committers orderedStringSet
	lineByLine("git log '--format=%cN <%cE>'", "git committer fetch", func(line string) error {
		committers.Add(strings.TrimSpace(line))
		return nil
	})
	return committers
}

func (vcs *VCS) commitNote(text string) error {
	if !vcs.testclock.IsZero() {
		vcs.testclock = vcs.testclock.Add(time.Second * 10)
		os.Setenv("GIT_AUTHOR_DATE", rfc3339(vcs.testclock))
	}

	// Attaches the note to the current head object */
	_, err := captureFromProcess("git notes --ref shimmer append -m '" + text + "'")
	return err
}

func (vcs *VCS) getNotes() (string, error) {
	stdout, cmd, err1 := readFromProcess("git log --reverse --notes=shimmer --format=%N")
	if err1 != nil {
		return "", err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	var out string
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return out, fmt.Errorf("getLog: %s", err2)
		}
		out += line
	}
	return out, nil
}

func (vcs VCS) remotes() []string {
	remotes := make([]string, 0)
	err := lineByLine("git remote", "git remote", func(line string) error {
		remotes = append(remotes, strings.TrimSpace(line))
		return nil
	})
	if err != nil {
		return nil
	}
	return remotes
}

func (vcs VCS) sync(peer string) (string, error) {
	output, err := captureFromProcess("git fetch -q " + peer + " refs/notes/shimmer:refs/notes/" + peer + "/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync fetch: %s", err)
	}
	os.Setenv("GIT_NOTES_REF", "refs/notes/shimmer")
	output, err = captureFromProcess("git notes merge -q -s union " + peer + "/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync merge: %s", err)
	}
	output, err = captureFromProcess("git push -q " + peer + " refs/notes/shimmer")
	if err != nil {
		return output, fmt.Errorf("sync push: %s", err)
	}
	return output, nil
}

func (vcs VCS) commit(committer string, name string, content string, comment string) error {
	var err error

	c, err := mail.ParseAddress(committer)
	if err != nil {
		return fmt.Errorf("invalid committer address: %s", err)
	}

	doAdd := !exists(name)

	file, err := os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return fmt.Errorf("OpenFile in makeCommit: %s", err)
	}
	file.WriteString(content)
	file.Close()

	if doAdd {
		_, err = captureFromProcess("git add " + name)
		if err != nil {
			return fmt.Errorf("add makeCommit: %s", err)
		}
	}

	os.Setenv("GIT_COMMITTER_NAME", c.Name)
	os.Setenv("GIT_COMMITTER_EMAIL", c.Address)
	_, err = captureFromProcess("git commit -q -m '" + comment + "' " + name)
	if err != nil {
		return fmt.Errorf("running git commit: %s", err)
	}
	return nil
}

// clone works around a bug in get still present in 2.30.2; cloning a
// repository doesn't pull its notes or the config content on the
// banch. This necessites a multiple-command workaround.
func (vcs VCS) clone(peer string, local string) error {
	captureFromProcess(fmt.Sprintf("git clone -q %s %s", peer, local))
	if !vcs.isRepository(local) {
		return fmt.Errorf("Clone of %s to %s failed", peer, local)
	}
	os.Chdir(local)
	defer os.Chdir("..")
	currentBranch, err := vcs.currentBranch()
	captureFromProcess("git checkout -q shimmer")
	captureFromProcess("git fetch -q origin refs/notes/*:refs/notes/*")
	if err == nil {
		captureFromProcess("git checkout -q " + currentBranch)
	}
	return nil
}

func (vcs *VCS) setConfig(name string, content string, comment string) error {
	if !vcs.isRepository(".") {
		return fmt.Errorf("setConfig didn't see a repository directory")
	}
	err := yaml.Unmarshal([]byte(content), make(map[interface{}]interface{}))
	if err != nil {
		return fmt.Errorf("unmarshaling %q: %s", content, err)
	}
	currentBranch, err := vcs.currentBranch()
	if err != nil {
		return fmt.Errorf("setConfig can't get current branch")
	}
	_, err = captureFromProcess("git checkout -q --orphan shimmer")
	if err != nil {
		return fmt.Errorf("failed to switch to configuration branch")
	}
	if !exists("config") {
		// This is is what we really don't want to get to unless we're
		// sure the branch change has occurred.  It should only run
		// the first time we come here, before the config file is
		// initially created.
		captureFromProcess("git rm -fr .")
	}
	vcs.commit(name, "config", content, comment)
	captureFromProcess("git checkout -q " + currentBranch)
	return nil
}

type shimmerConfig struct {
	Path         string
	Description  string
	NotifyEnable bool
	CloneURL     string
	Error        string `json:",omitempty"`
}

func (sc shimmerConfig) makeCloneURL() string {
	// Only one slash on the prefix because repodir is already absolute
	return fmt.Sprintf("https:/%s/%s", repodir, sc.Path)
}

const configDumper = "git cat-file blob shimmer:config"

func (vcs *VCS) getConfig(dir string, w io.Writer) {
	if !vcs.isRepository(dir) {
		croak("parseConfig didn't see a repository directory")
	}
	pwd, err := os.Getwd()
	if err != nil {
		croak("getConfig can't get config directory: %s", err)
	}
	os.Chdir(dir)
	defer os.Chdir(pwd)
	ifp, _, err := readFromProcess(configDumper)
	if err != nil {
		croak("getConfig file fetch failure: %s", err)
	}
	defer ifp.Close()
	_, err = io.Copy(w, ifp)
	if err != nil {
		croak("getConfig file copy: %s", err)
	}
}

func (vcs *VCS) parseConfig(dir string) shimmerConfig {
	out := shimmerConfig{Path: dir, Description: "(no description)"}

	if !vcs.isRepository(dir) {
		out.Error = "parseConfig didn't see a repository directory"
		return out
	}

	pwd, err := os.Getwd()
	if err != nil {
		out.Error = fmt.Sprintf("parseConfig can't get config directory: %s", err)
		return out
	}
	os.Chdir(dir)
	defer os.Chdir(pwd)

	ifp, _, err := readFromProcess(configDumper)
	if err != nil {
		out.Error = fmt.Sprintf("parseConfig file fetch failure: %s", err)
		return out
	}
	defer ifp.Close()

	data, err := ioutil.ReadAll(ifp)
	if err != nil {
		out.Error = fmt.Sprintf("parseConfig file read failure: %s", err)
		return out
	}

	dict := make(map[interface{}]interface{})
	err = yaml.Unmarshal(data, &dict)
	if err != nil {
		out.Error = fmt.Sprintf("unmarshaling %q: %s", data, err)
		return out
	}

	// Interpretation of the config directory begins here
	out.Description = fmt.Sprintf("%s", dict["description"])
	if n, ok := dict["notify_enable"]; !ok {
		out.NotifyEnable = true
	} else if b, ok := n.(bool); ok {
		out.NotifyEnable = b
	} else {
		out.Error = fmt.Sprintf("unmarshaling %q: %s", data, err)
		return out
	}
	return out
}

func getProperty(prop string) string {
	fp, _, _ := readFromProcess("git config " + prop)
	defer fp.Close()
	val, _ := ioutil.ReadAll(fp)
	return string(val)
}

/* Top layer: shimmer command logic */

type orderedStringSet []string

var nullOrderedStringSet orderedStringSet

func newOrderedStringSet(elements ...string) orderedStringSet {
	set := make([]string, 0)
	for _, el := range elements {
		found := false
		for _, already := range set {
			if already == el {
				found = true
			}
		}
		if !found {
			set = append(set, el)
		}
	}
	return set
}

// Listify bares the underlying map so we can iterate over its keys
func (s orderedStringSet) Listify() []string {
	return s
}

func (s orderedStringSet) Contains(item string) bool {
	for _, el := range s {
		if item == el {
			return true
		}
	}
	return false
}

func (s *orderedStringSet) Remove(item string) bool {
	for i, el := range *s {
		if item == el {
			// Zero out the deleted element so it's GCed
			copy((*s)[i:], (*s)[i+1:])
			(*s)[len(*s)-1] = ""
			*s = (*s)[:len(*s)-1]
			return true
		}
	}
	return false
}

func (s *orderedStringSet) Add(item string) {
	for _, el := range *s {
		if el == item {
			return
		}
	}
	*s = append(*s, item)
}

func (s orderedStringSet) Subtract(other orderedStringSet) orderedStringSet {
	var diff orderedStringSet
	for _, outer := range s {
		for _, inner := range other {
			if outer == inner {
				goto dontadd
			}
		}
		diff = append(diff, outer)
	dontadd:
	}
	return diff
}

func (s orderedStringSet) Intersection(other orderedStringSet) orderedStringSet {
	// Naive O(n**2) method - don't use on large sets if you care about speed
	var intersection orderedStringSet
	for _, item := range s {
		if other.Contains(item) {
			intersection = append(intersection, item)
		}
	}
	return intersection
}

func (s orderedStringSet) Union(other orderedStringSet) orderedStringSet {
	var union orderedStringSet
	union = s[:]
	for _, item := range other {
		if !s.Contains(item) {
			union = append(union, item)
		}
	}
	return union
}

func (s orderedStringSet) String() string {
	if len(s) == 0 {
		return "[]"
	}
	var rep strings.Builder
	rep.WriteByte('[')
	lastIdx := len(s) - 1
	for idx, el := range s {
		fmt.Fprintf(&rep, "\"%s\"", el)
		if idx != lastIdx {
			rep.WriteString(", ")
		}
	}
	rep.WriteByte(']')
	return rep.String()
}

func (s orderedStringSet) Equal(other orderedStringSet) bool {
	if len(s) != len(other) {
		return false
	}
	// Naive O(n**2) method - don't use on large sets if you care about speed
	for _, item := range s {
		if !other.Contains(item) {
			return false
		}
	}
	return true
}

func (s orderedStringSet) Empty() bool {
	return len(s) == 0
}

func setify(header string) orderedStringSet {
	var out orderedStringSet
	if header != "" {
		commasRemoved := strings.ReplaceAll(header, ",", " ")
		for _, token := range strings.Fields(commasRemoved) {
			out.Add(strings.TrimSpace(token))
		}
	}
	return out
}

// Engine is a command interpreter primitve for the storage manager
type Engine struct {
	vcs *VCS
}

func addressesString(addresses []*mail.Address) string {
	out := ""
	for _, address := range addresses {
		out += strings.Replace(address.String(), `"`, ``, -1) + ", "
	}
	if strings.HasSuffix(out, ", ") {
		out = out[:len(out)-2]
	}
	return strings.Replace(out, `"`, ``, -1)
}

func (engine *Engine) generateActionStamp() string {
	if !engine.vcs.testclock.IsZero() {
		return rfc3339(engine.vcs.testclock) + "!" + engine.vcs.user.Address
	}
	return rfc3339(time.Now()) + "!" + engine.vcs.user.Address
}

type parsedRequest struct {
	from, threadTags, id string
	addressees           []*mail.Address
	mergeRequest         string
	headers, body        string
}

func (update *parsedRequest) prependHeader(header string, value string) {
	update.headers = header + ": " + value + "\n" + update.headers
}

func (update *parsedRequest) appendHeader(header string, value string) {
	update.headers += header + ": " + value + "\n"
}

func (engine *Engine) requestParser(in io.Reader, threadID string, user string) (parsedRequest, error) {
	var parsed parsedRequest
	var subject string

	if user == "" {
		user = engine.vcs.userid()
	}

	message, err := mail.ReadMessage(in)
	if err != nil {
		return parsed, err
	}

	header := message.Header
	stash := ""

	// Cumulates over its threads
	for _, hdr := range []string{"In-Reply-To", "X-Assigned-To"} {
		if payload := header.Get(hdr); payload != "" {
			addresses, err := mail.ParseAddressList(payload)
			if err != nil {
				return parsed, fmt.Errorf("bad %s header in message input: %s", hdr, err)
			}
			stash += hdr + ": " + addressesString(addresses) + "\n"
		}
	}

	if subject = header.Get("Subject"); subject != "" {
		stash += "Subject: " + subject + "\n"
	}

	// If we're ever going to do tag validation, it should go here

	if parsed.threadTags = header.Get("X-Message-Tags"); parsed.threadTags != "" {
		stash += "X-Message-Tags: " + parsed.threadTags + "\n"
	}

	if visibility := header.Get("X-Visibility"); visibility != "" {
		stash += "X-Visibility: " + visibility + "\n"
	}

	if action := header.Get("X-Subscription"); action != "" {
		stash += "X-Subscription: " + action + " \n"
	}

	if parsed.mergeRequest = header.Get("X-Merge-Request"); parsed.mergeRequest != "" {
		stash += "X-Merge-Request: " + parsed.mergeRequest + "\n"
	}

	if markup := header.Get("X-Markup"); markup != "" {
		if markup != "literal" && markup != "markdown" && markup != "asciidoc" {
			return parsed, fmt.Errorf("only literal, markdown, and asciidoc formats are supported")
		}
		stash += "X-Markup: " + markup + "\n"
	}

	// We need to preserve these for messages with attachments
	for _, hd := range []string{"MIME-Version", "Content-Type"} {
		if hv := header.Get(hd); hv != "" {
			stash += hd + ": " + hv + "\n"
		}
	}

	body, err := io.ReadAll(message.Body)
	if err != nil {
		return parsed, err
	}

	parsed.headers = stash
	parsed.id = "<" + engine.generateActionStamp() + ">"
	parsed.body = string(body)

	// We're using the standard RFC5322 date format here,  It carries
	// a risk if two messages with inheritable control headers arrive
	// in the same second, the wrong one could be applied to later
	// messages.
	parsed.prependHeader("Message-ID", parsed.id)
	parsed.prependHeader("Date", engine.vcs.time().Format(time.RFC1123Z))
	parsed.prependHeader("From", user)

	// Be careful not to move X-Message-Tags before Date; if you do that
	// the logic for capturing most recent update times will fail subtly.

	if parsed.threadTags == "" {
		if threadID == "" {
			if parsed.mergeRequest != "" {
				threadID = "merge-request:" + engine.generateActionStamp()
			} else {
				threadID = "issue:" + engine.generateActionStamp()
			}
		}
		parsed.threadTags = threadID
		parsed.appendHeader("X-Message-Tags", threadID)
	}

	// Someday, don't do this in the webserver - the selection is pre-validated there
	if assignees := message.Header.Get("X-Assigned-To"); assignees != "" {
		committers := engine.vcs.getCommitters()
		for _, assignee := range strings.Split(assignees, ",") {
			if !committers.Contains(strings.TrimSpace(assignee)) {
				return parsed, fmt.Errorf("assignees must be committers")
			}
		}
	}

	return parsed, nil
}

func (update parsedRequest) dump() string {
	return update.headers + fmt.Sprintf("Content-Length: %d\n", len(update.body)) + "\n" + update.body
}

func pushNotify(message, subscribers string) error {
	// Simeday, support more push types:
	// IRC via a local irkerd instance is an obvious one.
	mta := "sendmail -t"
	if configMTA := getProperty("shimmer.mta"); configMTA != "" {
		mta = configMTA
	}
	if envMTA := os.Getenv("SHIMMER_MTA"); envMTA != "" {
		mta = envMTA
	}
	wfp, cmd, err := writeToProcess(mta)
	if err != nil {
		return fmt.Errorf("pushNotify: %s", err)
	}
	wfp.Write([]byte("To: " + subscribers + "\n" + message))
	wfp.Close()
	cmd.Wait()

	return nil
}

// Next three lines span the complete set of headers interprerted by shimmer */
var preserveHeaders orderedStringSet = orderedStringSet{
	"From",
	"Date",
	"X-Subscription",
	"X-Message-Tags",
}
var replacingHeaders orderedStringSet = orderedStringSet{
	"Subject",
	"X-Assigned-To",
	"X-Merge-Request",
	"X-Visibility",
	"X-Markup",
}
var discardHeaders orderedStringSet = orderedStringSet{
	"In-Reply-To",
	"Message-ID",
	"MIME-Version",
	"Content-Type",
}
var sensitiveHeaders orderedStringSet = orderedStringSet{"From", "X-Merge-Request", "X-Subscription"}
var headerOrder = []string{
	"From",
	"Date",
	"Message-ID",
	"Subject",
	"In-Reply-To",
	"X-Message-Tags",
	"X-Visibility",
	"X-Assigned-To",
	"X-Subscription",
	"X-Merge-Request",
	"X-Markup",
	"MIME-Version",
	"Content-Type",
}

type datedMessage struct {
	mtext     string
	tags      orderedStringSet
	date      time.Time
	from      *mail.Address
	committed bool
	inherited map[string]bool
}

// AttachmentPart represents an attachment part
type AttachmentPart struct {
	Content  string
	Filename string
}

type unpackedMessage struct {
	Committed       bool             `json:",omitempty"`
	From            *mail.Address    `json:",omitempty"`
	Date            *time.Time       `json:",omitempty"`
	Markup          string           `json:",omitempty"`
	MessageID       string           `json:",omitempty"`
	Subject         string           `json:",omitempty"`
	NewSubject      bool             `json:",omitempty"`
	InReplyTo       string           `json:",omitempty"`
	MessageTags     orderedStringSet `json:",omitempty"`
	NewMessageTags  bool             `json:",omitempty"`
	Visibility      string           `json:",omitempty"`
	NewVisibility   bool             `json:",omitempty"`
	AssignedTo      *mail.Address    `json:",omitempty"`
	NewAssignedTo   bool             `json:",omitempty"`
	Subscription    string           `json:",omitempty"`
	NewSubscription bool             `json:",omitempty"`
	MergeRequest    string           `json:",omitempty"`
	NewMergeRequest bool             `json:",omitempty"`
	Error           string           `json:",omitempty"`
	Body            string           `json:",omitempty"`
	Attachments     []AttachmentPart `json:",omitempty"`
}

func unpack(mtext string) unpackedMessage {
	var out unpackedMessage
	var err error
	if strings.TrimSpace(mtext) == "" {
		out.Error = "unexpectedly empty message - should never happen"
		return out
	}

	msg, err := mail.ReadMessage(strings.NewReader(mtext))
	if err != nil {
		out.Error = fmt.Sprintf("while parsing message block: %s", err)
		return out
	}
	hd := msg.Header

	// The next two parse calls are inefficient - where unpack is
	// called we've done this before.  Writing this function as a
	// method of datedMessage would evade this, but make it
	// tricky to unit-test, and it's a plausible enough point of
	// failure for that to be unacceptable.
	if from := hd.Get("From"); from != "" {
		a, err := mail.ParseAddress(from)
		if err == nil {
			out.From = a
		} else {
			out.Error = fmt.Sprintf("while parsing from address: %s", err)
			return out
		}
	}
	if rawdate := hd.Get("Date"); rawdate != "" {
		date, err := time.Parse(time.RFC1123Z, rawdate)
		if err == nil {
			out.Date = &date
		} else {
			out.Error = fmt.Sprintf("while parsing message date: %s", err)
			return out
		}
	}

	out.MessageID = hd.Get("Message-ID")
	out.Subject = hd.Get("Subject")
	out.InReplyTo = hd.Get("In-Reply-To")
	if tags := hd.Get("X-Message-Tags"); tags != "" {
		out.MessageTags = setify(tags)
	}
	out.Visibility = hd.Get("X-Visibility")
	if assignedto := hd.Get("X-Assigned-To"); assignedto != "" {
		b, err := mail.ParseAddress(assignedto)
		if err != nil {
			out.Error = fmt.Sprintf("while parsing assignedto address: %s", err)
			return out
		}
		out.AssignedTo = b
	}
	out.Subscription = hd.Get("X-Subscription")
	out.MergeRequest = hd.Get("X-Merge-Request")
	out.Markup = hd.Get("X-Markup")

	if ct := hd.Get("Content-Type"); ct == "" {
		ob, err := io.ReadAll(msg.Body)
		if err == nil {
			out.Body = string(ob)
		} else {
			out.Error = fmt.Sprintf("while reading message body: %s", err)
			return out
		}
	} else {
		_, params, err := mime.ParseMediaType(ct)
		if err != nil {
			out.Error = fmt.Sprintf("invalid content-Type lineL %q", ct)
			return out
		}
		r := multipart.NewReader(msg.Body, params["boundary"])
		textPart, err := r.NextPart()
		if err != nil {
			out.Error = "missing text part"
			return out
		}
		slurp, err := io.ReadAll(textPart)
		if err != nil {
			out.Error = "missing text body"
			return out
		}
		out.Body = string(slurp)

		for {
			p, err := r.NextPart()
			if err == io.EOF {
				break
			}
			if err != nil {
				out.Error = "missing attachment part"
				return out
			}
			slurp, err := io.ReadAll(p)
			if err != nil {
				out.Error = "missing attachment body"
				return out
			}
			out.Attachments = append(out.Attachments, AttachmentPart{Content: base64.URLEncoding.EncodeToString(slurp), Filename: p.FileName()})
		}
	}

	return out
}

// getText retrieves the test (headers and body in RFC 5322 format) of the datedMessage
func (m datedMessage) getText() string {
	// This is a method because someday we're going to
	// need to not have to store all message traffic
	// in core, at which point this will redeem a
	// promise.
	return m.mtext
}

// setText sets the text (headers and body in RRFC 5322 format) of the datedMessage
func (m *datedMessage) setText(s string) {
	// This is a method because someday we're going to
	// need to not have to store all messaage traffic
	// in core, at which point this will store a
	// promise.
	m.mtext = s
}

func (m *datedMessage) messageSelectedBy(selector string) bool {
	for _, possible := range m.tags {
		if tagSelectedBy(selector, possible) {
			return true
		}
	}
	return false
}

type datedThread struct {
	Tag          string
	Began        time.Time
	Updated      time.Time
	Subject      string
	MergeRequest string
	Originator   *mail.Address
	AssignedTo   *mail.Address `json:",omitempty"`
	Count        int
	Visible      bool
	IsEcho       bool `json:"-"` // Used for HTML templating
	IsMerge      bool `json:"-"` // Used for HTML templating
}

type shimmerState struct {
	messages     []datedMessage
	taglist      []datedThread
	headersByTag map[string]mail.Header // This where we gather replacing headers
	tagcount     map[string]int
	updates      map[string]time.Time
	tagsbyname   map[string]*datedThread
}

// loadShimmerState pulls all selected messages into memory.
// This is very convenient, but risks blowing up the working set
// to absurd size on large projects.  If this ever becomes an issue,
// reimplement so the internal structure stores promises - ways to
// retrieve messages rather than the messages themselves.  No point
// in doing this until someone reports an OOM, however.
func (engine *Engine) loadShimmerState(threadFilter string, alltext bool) (shimmerState, error) {
	if threadFilter == "" {
		threadFilter = "all"
	}
	//fmt.Fprintf(os.Stderr, "loadShimmerState(%q, %v)\n", threadFilter, alltext)
	var state shimmerState
	var zeroMessage datedMessage

	var err error
	state.headersByTag = make(map[string]mail.Header)
	state.tagcount = make(map[string]int)
	state.updates = make(map[string]time.Time)

	processSource := func(text io.Reader, committed bool) error {
		var acc datedMessage
		var keep bool
		var headerLatch int
		var expected, bodycount int
		startmessage := func() {
			acc = zeroMessage
			acc.committed = committed
			acc.tags = nullOrderedStringSet
			keep = false
			headerLatch = 0
		}
		endmessage := func() {
			//fmt.Fprintf(os.Stderr, "Endmsg: %q\n", acc.mtext)
			if !acc.tags.Empty() && keep {
				// Necessary because git log --format=%N generates extraneous
				// linefeeds for messages without notes.  The second \n guarantees
				// a spacer line after each message.
				if alltext {
					acc.mtext = strings.TrimSpace(acc.mtext) + "\n\n"
				} else {
					// \n added so ReadMessage isn't confused by
					// the body being absent.
					//fmt.Fprintf(os.Stderr, "Truncating\n", acc.mtext)
					acc.mtext = acc.mtext[:headerLatch] + "\n"
				}
				//fmt.Fprintf(os.Stderr, "Creating message block with text: %q\n", acc.mtext)
				state.messages = append(state.messages, acc)
			}
		}
		startmessage()
		scanner := bufio.NewScanner(text)
		for scanner.Scan() {
			line := scanner.Text() + "\n"
			//fmt.Fprintf(os.Stderr, "Line in: %q\n", line)
			if strings.HasPrefix(line, "From: ") && expected <= bodycount {
				endmessage()
				startmessage()
				acc.from, _ = mail.ParseAddress(strings.TrimSpace(line[6:]))
			} else if headerLatch == 0 {
				getPayload := func(line string, prefix string) (string, bool) {
					if !strings.HasPrefix(line, prefix+": ") {
						return "", false
					}
					return strings.TrimSpace(line[len(prefix)+2:]), true
				}
				if payload, ok := getPayload(line, "X-Message-Tags"); ok {
					acc.tags = setify(payload)
					if threadFilter == "pending" {
						keep = !committed
					} else {
						keep = tagSelectedBy(threadFilter, payload)
					}
					for _, tag := range acc.tags.Listify() {
						state.tagcount[tag]++
						if state.updates[tag].Before(acc.date) {
							state.updates[tag] = acc.date
						}
					}
				} else if payload, ok := getPayload(line, "Date"); ok {
					acc.date, err = time.Parse(time.RFC1123Z, payload)
					if err != nil {
						return fmt.Errorf("while parsing Date line: %s", err)
					}
				} else if payload, ok := getPayload(line, "Content-Length"); ok {
					expected, _ = strconv.Atoi(strings.TrimSpace(payload))
				} else if strings.TrimSpace(line) == "" {
					headerLatch = len(acc.mtext)
					bodycount = 0
				}
			} else {
				bodycount += len(line)
			}
			//fmt.Fprintf(os.Stderr, "Appending: %q to %q\n", line, acc.mtext)
			acc.mtext += line
		}
		if err == nil {
			endmessage()
		}
		return err
	}

	// First, committed traffic from the repo
	text, _, err := readFromProcess("git log --reverse --notes=shimmer --format=%N")
	if err != nil {
		return state, err
	}
	if err = processSource(text, true); err != nil {
		return state, fmt.Errorf("loadShimmerState failure: %s", err)
	}

	// Then, messages from the local queue
	spooldir := filepath.Join(engine.vcs.repoDirectory(), "shimmer", "spool")
	if under, err := ioutil.ReadDir(spooldir); err == nil {
		for _, f := range under {
			fp, err := os.Open(filepath.Join(spooldir, f.Name()))
			if err == nil {
				processSource(fp, false)
				fp.Close()
			}
		}
	}

	// Necessary because it's not guaranteed that messagews
	// arrived in time order
	sort.Slice(state.messages, func(i, j int) bool {
		return state.messages[i].date.Before(state.messages[j].date)
	})

	// Now that the messages are time-ordered, we can
	// ship then in date sequence, doing header inheritance
	for i, item := range state.messages {
		r := strings.NewReader(item.getText())
		m, err := mail.ReadMessage(r)
		if err != nil {
			return state, err
		}

		var body []byte
		if alltext {
			body, err = io.ReadAll(m.Body)
			if err != nil {
				return state, fmt.Errorf("reading message body: %s", err)
			}
		}

		// Use of Get means all headers after the first with a given
		// header specifier are ignored

		tags := make([]string, 0)
		if tagline := m.Header.Get("X-Message-Tags"); len(tagline) > 0 {
			tags = setify(tagline)
		}

		// This is where header inheritance is done
		out := ""
		state.messages[i].inherited = make(map[string]bool)
		for _, hd := range headerOrder {
			hdrval := m.Header.Get(hd)
			for _, tag := range tags {
				inherited := state.headersByTag[tag]
				if len(inherited[hd]) > 0 {
					old := inherited[hd][0]
					if replacingHeaders.Contains(hd) && len(hdrval) == 0 {
						state.messages[i].inherited[hd] = true
						hdrval = old
					}
				}
			}
			if hdrval != "" {
				m.Header[hd] = []string{hdrval}
				out += hd + ": " + hdrval + "\n"
			}
		}

		out += "\n"
		if alltext {
			out += string(body)
		}

		// Copy header's directory keys and content, not just a
		// reference. This is so we guarantee can't screw ourselves
		// with side effects.  Might not be necessary but it's
		// cheap insurance.
		for _, tag := range tags {
			state.headersByTag[tag] = make(mail.Header)
			for k, v := range m.Header {
				state.headersByTag[tag][k] = v
			}
		}

		state.messages[i].setText(out)
	}

	return state, nil
}

var threadNamespaces []string = []string{"issues", "merge-requests"}

func tagSelectedBy(selector string, tags string) bool {
	for _, tag := range setify(tags) {
		if selector == "all" || tag == selector {
			return true
		}

		namespaced := false
		matched := false
		reqprefix := selector[:len(selector)-1] + ":"
		for _, namespace := range threadNamespaces {
			if strings.HasPrefix(tag, reqprefix) {
				matched = true
				namespaced = true
				break
			}
			if strings.HasPrefix(tag, namespace[:len(namespace)-1]+":") {
				namespaced = true
			}
		}
		if matched || (!namespaced && selector == "echoes") {
			return true
		}
	}
	return false
}

func (state *shimmerState) indexTags() {
	if state.taglist == nil {
		state.taglist = make([]datedThread, 0)
		seen := newOrderedStringSet()
		// Messages are sorted by date
		for _, m := range state.messages {
			newtags := m.tags.Subtract(seen)
			seen = seen.Union(newtags)
			for _, tag := range newtags.Listify() {
				addr, _ := mail.ParseAddress(state.headersByTag[tag].Get("X-Assigned-To"))
				isEcho := !strings.HasPrefix(tag, "issue:") && !strings.HasPrefix(tag, "merge-request:")
				isMerge := strings.HasPrefix(tag, "merge-request:")
				state.taglist = append(state.taglist,
					datedThread{
						Tag:          tag,
						Subject:      state.headersByTag[tag].Get("Subject"),
						MergeRequest: state.headersByTag[tag].Get("X-Merge-Request"),
						AssignedTo:   addr,
						Began:        m.date,
						Updated:      state.updates[tag],
						Originator:   m.from,
						Count:        state.tagcount[tag],
						Visible:      state.isVisible(newtags),
						IsEcho:       isEcho,
						IsMerge:      isMerge,
					})
			}
		}
		sort.Slice(state.taglist, func(i int, j int) bool {
			return state.taglist[i].Began.Before(state.taglist[j].Began)
		})
		state.tagsbyname = make(map[string]*datedThread)
		for i, item := range state.taglist {
			state.tagsbyname[item.Tag] = &state.taglist[i]
		}
	}
}

// tagsByDate returns a list of thread starts by date. The origin
// sate and originating user is reported, also the thread's final
// Subject, Assigned-To, and visibility status.
func (state shimmerState) tagsByDate(all bool) []datedThread {
	taglist := make([]datedThread, 0)
	state.indexTags()
	for _, tagentry := range state.taglist {
		if tagentry.Visible || all {
			taglist = append(taglist, tagentry)
		}
	}
	return taglist
}

func (state shimmerState) isVisible(tags orderedStringSet) bool {
	// This tag set is visible if any tag in it is visible
	for _, tag := range tags.Listify() {
		hd, ok := state.headersByTag[tag]
		if !ok {
			return true
		}
		if hd.Get("X-Visibility") != "hide" {
			return true
		}
	}

	return false
}

func (engine *Engine) cat(threadFilter string, out io.Writer) error {
	state, err := engine.loadShimmerState(threadFilter, true)
	if err != nil {
		return fmt.Errorf("cat: %s", err)
	}
	for _, m := range state.messages {
		out.Write([]byte(m.getText()))
	}
	return nil
}

func (engine *Engine) mbox(threadFilter string, out io.Writer) error {
	state, err := engine.loadShimmerState(threadFilter, true)
	if err != nil {
		return fmt.Errorf("mbox: %s", err)
	}
	for _, m := range state.messages {
		// RFC 4155 conformance
		ts := m.date.UTC().Format(time.UnixDate)
		ts = strings.Replace(ts, "UTC ", "", 1)
		out.Write([]byte(fmt.Sprintf("From shimmer@%s %s\n", hostname, ts)))
		out.Write([]byte(m.getText()))
	}
	return nil
}

func (engine *Engine) subscriptionList(threadFilter string) string {
	state, err := engine.loadShimmerState(threadFilter, false)
	if err != nil {
		return ""
	}
	list := newOrderedStringSet()
	for _, m := range state.messages {
		r := strings.NewReader(m.getText())
		m, err := mail.ReadMessage(r)
		if err != nil {
			continue
		}

		// Assigned-To has to be processed early so it can be negated by From/X-Subscription
		addrList, err := mail.ParseAddressList(m.Header.Get("X-Assigned-To"))
		if err == nil {
			for _, addr := range addrList {
				list.Add(strings.Replace(addr.String(), `"`, ``, -1))
			}
		}

		from := m.Header.Get("From")
		action := m.Header.Get("X-Subscription")
		if strings.Contains(action, "leave") {
			list.Remove(from)
		} else {
			list.Add(from)
		}
	}
	return strings.Join(list.Listify(), ", ")
}

func (engine *Engine) enqueue(parsedUpdate parsedRequest) error {
	if !engine.vcs.isRepository(".") {
		return errors.New("attempting to enque in a non-repository")
	}
	spooldir := filepath.Join(engine.vcs.repoDirectory(), "shimmer", "spool")
	if !exists(spooldir) {
		if err := os.Mkdir(filepath.Join(engine.vcs.repoDirectory(), "shimmer"), userReadWriteSearchMode); err != nil {
			return fmt.Errorf("while creating Shimmer spool directory: %s", err)
		}
		if err := os.Mkdir(filepath.Join(engine.vcs.repoDirectory(), "shimmer", "spool"), userReadWriteSearchMode); err != nil {
			return fmt.Errorf("while creating Shimmer spool directory: %s", err)
		}
	}
	return ioutil.WriteFile(filepath.Join(spooldir, parsedUpdate.id), []byte(parsedUpdate.dump()), 0644)
}

func (vcs VCS) findNearbyRepositories() (map[string]shimmerConfig, error) {
	repositories := make(map[string]shimmerConfig)
	under, err := ioutil.ReadDir("./")
	if err != nil {
		return nil, fmt.Errorf("while finding repositories: %s", err)
	}
	for _, f := range under {
		subdir := f.Name()
		if vcs.isRepository(subdir) {
			config := vcs.parseConfig(subdir)
			repositories[subdir] = config
		}
	}
	if len(repositories) == 0 && vcs.isRepository(".") {
		repositories["."] = vcs.parseConfig(".")
	}
	return repositories, nil
}

func edit(engine *Engine, s tcell.Screen, threadID string, explanation string) (*parsedRequest, error) {
	var err error
	var source *os.File = os.Stdin

	if editor := os.Getenv("EDITOR"); editor != "" && term.IsTerminal(int(os.Stdin.Fd())) {
		source, err = ioutil.TempFile("/tmp", "shimmer")
		if err != nil {
			return nil, fmt.Errorf("Preparation to edit failed: %s", err)
		}
		defer os.Remove(source.Name())
		if explanation != "" {
			source.WriteString(explanation + "# All lines beginning with # will be removed before the message is sent.\n")
		}
		if s != nil {
			s.Suspend()
			defer s.Resume()
		}
		_, err = captureFromProcess(editor + " " + source.Name())
		if err != nil {
			return nil, fmt.Errorf("Editing failed: %s", err)
		}
		source.Seek(0, io.SeekStart)
	}
	parsedUpdate, err := engine.requestParser(source, threadID, "")
	if err != nil {
		return nil, fmt.Errorf("Message parse failed: %s", err)
	}
	re := regexp.MustCompile(`(\n|^)#.*\n`)
	for {
		old := parsedUpdate.body
		parsedUpdate.body = re.ReplaceAllString(parsedUpdate.body, "${1}")
		if parsedUpdate.body == old {
			break
		}
	}
	if err = engine.vcs.commitNote(parsedUpdate.dump()); err != nil {
		return nil, fmt.Errorf("Message submission failed: %s", err)
	}
	return &parsedUpdate, nil
}

/* The TUI browser */

func linewrap(line string, ymax int) []string {
	out := make([]string, 0)
	if len(line) == 0 {
		return []string{""}
	}
	s := 0
	i := 0
	isspace := func(c byte) bool {
		// We're deliberately ignoring Unicode complications here.
		// See https://jkorpela.fi/chars/spaces.html
		return c == ' ' || c == '\t'
	}
	for i < len(line) {
		nextSpace := func(i int) int {
			// Stop at EOL or next space, whichever is sooner.
			// Note, i may end as an invakid index.
			for i < len(line) && !isspace(line[i]) {
				i++
			}
			return i
		}

		if i >= len(line)-1 || (isspace(line[i]) && !isspace(line[i+1]) && nextSpace(i+1)-s >= ymax) {
			out = append(out, line[s:i+1])
			s = i
		}
		i++
	}

	return out
}

func tui(engine *Engine, threadFilter string) {

	statusline := func(pwd string, threadSelect string, unhide bool) string {
		//complain("into statusline(%q, %q, %v)", pwd, threadSelect, unhide)
		vis := ""
		if unhide {
			vis = " (all)"
		}
		status := "---Shimmer: " + pwd + " " + threadSelect + vis
		//complain("after status line of '%d' chars", len(status))
		return status
	}

	indexDescription := func(state shimmerState, thread datedThread, maxwidth int) string {
		return state.headersByTag[thread.Tag].Get("Subject")
	}

	topline := func(inText bool) string {
		if inText {
			return "q:Quit r:Reply u:Unsubscribe v:Visibility i:Index"
		}
		return "q:Quit r:Reply u:Unsubscribe v:Visibility SPC:Select"
	}

	// No user-serviceable parts below this line

	if !term.IsTerminal(int(os.Stdin.Fd())) {
		croak("TUI requires standard input to be a terminal.")
	}
	pwd, err := os.Getwd()
	if err != nil {
		croak("can't get current directory: %s", err)
	}

	const GeneralHelpText = `Anywhere:

?                = display this command help
Ctrl-c, q, Esc   = quit
Ctrl-l           = force-refresh the display
PgDn, Down       = go to next screen-full of information
PgUp, Up         = return to previous screen-full of information
v                = toggle visibility of hidden notes
s                = do a message synchronization with upstream
l                = reload message state
r                = reply to thread
u                = unsubscribe from thread
o                = originate a thread

`
	const IndexHelpText = `Index page only:

Space, Enter, g  = visit thread
Up, p            = select previous thread
Down, n          = select next thread
Left, a          = pan listview left
Right, d         = pan listview right
`

	const ThreadHelpText = `Thread viewing only:

i      = go to index page
`

	// Initialize screen
	s, err := tcell.NewScreen()
	if err != nil {
		croak("error during screen allocation: %s", err)
	}

	if err := s.Init(); err != nil {
		croak("error during screen initialization: %s", err)
	}
	//s.EnableMouse()
	//s.EnablePaste()
	s.Clear()
	defer func() {
		maybePanic := recover()
		s.Fini()
		if maybePanic != nil {
			panic(maybePanic)
		}
		os.Exit(0)
	}()

	const (
		zoneBANNER     = 0
		zoneLIST   int = iota
		zoneTHREAD
		zoneSTATUS // Status line
		zoneERROR  // Errors and urgent messages
		zoneCOUNT  // Count
	)

	type zone struct {
		when        bool
		firstline   int
		lastline    int
		truncate    bool
		fill        string
		style       tcell.Style
		store       []string
		storeIndex  int
		selectIndex int
	}

	var zones [zoneCOUNT]zone

	mainsize := func(ind int) int {
		return zones[ind].lastline - zones[ind].firstline + 1
	}

	var mainSelectIndex, mainStoreIndex int

	bumpSelect := func(inThread bool, incr int) {
		z := &zones[zoneLIST]
		if inThread {
			z = &zones[zoneTHREAD]
		}
		// First step: Ensure the selection index is valid
		mainSelectIndex += incr
		if incr > 0 {
			if mainSelectIndex >= len(z.store)-1 {
				mainSelectIndex = len(z.store) - 1
			}
		}
		if incr < 0 {
			if mainSelectIndex < 0 {
				mainSelectIndex = 0
			}
		}
		// Second step: If this motion of the selector bar would
		// take it off-screen, change storeIndex until it's
		// still visible; this scrolls the display
		if mainSelectIndex < mainStoreIndex {
			mainStoreIndex = mainSelectIndex
		}
		if mainSelectIndex >= mainStoreIndex+(z.lastline-z.firstline+1) {
			mainStoreIndex = mainSelectIndex
		}
	}

	var errorText []string

	postError := func(base string, args ...interface{}) {
		errorText = strings.Split(strings.TrimSpace(fmt.Sprintf(base, args...)), "\n")
	}

	clearError := func() {
		errorText = nil
	}

	var inThread bool
	var unhide bool
	var leftstart int
	var threadSelect string
	var state shimmerState
	var activeThreadList []datedThread

	reply := func(threadID string) {
		explanation := fmt.Sprintf("Subject: %s\n\n# You are replying to thread %q.\n",
			state.headersByTag[threadID].Get("Subject"),
			threadID)
		p, err := edit(engine, s, threadID, explanation)
		if err == nil {
			postError("Reply to %s successful.", p.threadTags)
		} else {
			postError("While replying, %s", err)
		}
	}

	originate := func() {
		explanation := fmt.Sprint("Subject: \n\n# You are originating a new thread. A Subject line is required.\n")
		p, err := edit(engine, s, "", explanation)
		if err == nil {
			postError("Origination of %s successful.", p.threadTags)
		} else {
			postError("While originating, %s", err)
		}
	}

	unsubscribe := func(threadID string) {
		if !strings.Contains(engine.subscriptionList(threadID), engine.vcs.userid()) {
			postError("You are not subscribed to " + threadID)
			return
		}
		unsubscribe := strings.NewReader(fmt.Sprintf("X-Message-Tags: %s\nX-Subscription: leave\n\n", threadID))
		parsedUpdate, err := engine.requestParser(unsubscribe, "", "")
		if err != nil {
			postError("Composition failed: %s", err)
		} else {
			if err = engine.vcs.commitNote(parsedUpdate.dump()); err == nil {
				postError("Unsubscription from %s succeeded.", threadID)
				if !nomail && engine.vcs.parseConfig(".").NotifyEnable {
					pushNotify(parsedUpdate.dump(), engine.subscriptionList(parsedUpdate.threadTags))
				}
			} else {
				postError("Unsubscription failed: %s", err)
			}
		}
	}

	s.PostEvent(tcell.NewEventKey(tcell.KeyRune, rune('l'), 0))
	for {
		xmax, ymax := s.Size()
		zones = [zoneCOUNT]zone{
			// zoneBANNER
			{when: true,
				firstline: 0, lastline: 0, truncate: true, fill: " ",
				style: tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray),
				store: nil, storeIndex: 0, selectIndex: -1,
			},
			// zoneLIST
			{when: !inThread,
				firstline: 1, lastline: ymax - 2 - len(errorText), truncate: true, fill: " ",
				style: tcell.StyleDefault.Foreground(tcell.ColorReset).Background(tcell.ColorReset),
				store: nil, storeIndex: mainStoreIndex, selectIndex: mainSelectIndex,
			},
			// zoneTHREAD
			{when: inThread,
				firstline: 1, lastline: ymax - 2 - len(errorText), truncate: false, fill: " ",
				style: tcell.StyleDefault.Foreground(tcell.ColorReset).Background(tcell.ColorReset),
				store: nil, storeIndex: mainStoreIndex, selectIndex: -1,
			},
			// zoneSTATUS
			{when: true,
				firstline: ymax - 1 - len(errorText), lastline: ymax - 1 - len(errorText), truncate: false, fill: "-",
				style: tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray),
				store: nil, storeIndex: 0, selectIndex: -1,
			},
			// zoneERROR
			{when: errorText != nil,
				firstline: ymax - len(errorText), lastline: ymax - 1, truncate: false, fill: " ",
				style: tcell.StyleDefault.Foreground(tcell.ColorReset).Background(tcell.ColorReset),
				store: errorText, storeIndex: 0, selectIndex: -1,
			},
		}

		if inThread {
			zones[zoneTHREAD].store = make([]string, 0)
			for _, m := range state.messages {
				if m.tags.Contains(threadSelect) {
					for _, line := range strings.Split(m.getText(), "\n") {
						zones[zoneTHREAD].store = append(zones[zoneTHREAD].store, linewrap(line, xmax)...)
					}
				}
			}
			mainStoreIndex = 0
		} else {
			threadSelect = "[Thread index]"
			zones[zoneLIST].store = make([]string, 0)
			activeThreadList = state.tagsByDate(unhide)
			for _, thread := range activeThreadList {
				zones[zoneLIST].store = append(zones[zoneLIST].store, indexDescription(state, thread, xmax))
			}
		}
		zones[zoneBANNER].store = []string{topline(inThread)}
		zones[zoneSTATUS].store = []string{statusline(pwd, threadSelect, unhide)}

		for zi := range zones {
			zone := &zones[zi]
			if !zone.when || zone.store == nil {
				continue
			}
			for li := zone.firstline; li <= zone.lastline; li++ {
				text := ""
				within := li - zone.firstline
				if zone.storeIndex+within < len(zone.store) {
					text = zone.store[zone.storeIndex+within]
				}
				if xmax > len(text) {
					text += strings.Repeat(zone.fill, xmax-len(text))
				}
				style := zone.style
				if zi == zoneLIST && within == zone.selectIndex-zone.storeIndex {
					style = style.Reverse(true).Dim(true)
				}
				row := li
				col := 0
				for i, r := range []rune(text) {
					if i < leftstart {
						continue
					}
					s.SetContent(col, row, r, nil, style)
					col++
					if col >= xmax {
						if zone.truncate {
							break
						}
						row++
						col = 0
					}
					if row > zone.lastline {
						break
					}
				}
			}
		}

		// Update screen
		s.Show()

		// Poll event
		ev := s.PollEvent()

		clearError()

		// Process event
		switch ev := ev.(type) {
		case *tcell.EventResize:
			s.Clear()
			continue
		case *tcell.EventKey:
			if ev.Key() == tcell.KeyEscape || ev.Key() == tcell.KeyCtrlC || ev.Rune() == 'q' {
				return
			} else if ev.Key() == tcell.KeyCtrlL {
				s.Sync()
			} else if ev.Rune() == 'C' || ev.Rune() == 'c' {
				s.Clear()
			} else if ev.Rune() == ' ' || ev.Rune() == '\n' || ev.Rune() == 'g' {
				inThread = true
				threadSelect = activeThreadList[mainSelectIndex].Tag
			} else if ev.Rune() == 'i' {
				inThread = false
			} else if ev.Rune() == 'l' {
				state, err = engine.loadShimmerState(threadFilter, true)
				if err != nil {
					postError(fmt.Sprintf("error during shimmer state load: %s", err))
				}
			} else if ev.Rune() == 's' {
				out, err := engine.vcs.sync("origin")
				if err == nil {
					postError("sync successful")
					state, err = engine.loadShimmerState(threadFilter, true)
					if err != nil {
						postError(fmt.Sprintf("error during shimmer state reload: %s", err))
					}
				} else {
					postError(out)
				}
			} else if ev.Rune() == 'v' {
				unhide = !unhide
			} else if ev.Rune() == '?' {
				if inThread {
					postError(GeneralHelpText + ThreadHelpText)
				} else {
					postError(GeneralHelpText + IndexHelpText)
				}
			} else if ev.Key() == tcell.KeyUp || ev.Rune() == 'p' {
				if !inThread {
					bumpSelect(false, -1)
				} else {
					bumpSelect(true, -mainsize(zoneTHREAD))
				}
			} else if ev.Key() == tcell.KeyDown || ev.Rune() == 'n' {
				if !inThread {
					bumpSelect(false, 1)
				} else {
					bumpSelect(true, mainsize(zoneTHREAD))
				}
			} else if ev.Key() == tcell.KeyRight || ev.Rune() == 'd' {
				if !inThread {
					leftstart++
				}
			} else if ev.Key() == tcell.KeyLeft || ev.Rune() == 'a' {
				leftstart--
				if !inThread {
					if leftstart < 0 {
						leftstart = 0
					}
				}
			} else if ev.Key() == tcell.KeyPgUp {
				bumpSelect(inThread, -mainsize(zoneLIST))
			} else if ev.Key() == tcell.KeyPgDn {
				bumpSelect(inThread, mainsize(zoneLIST))
			} else if ev.Rune() == 'r' {
				selected := threadSelect
				if !inThread {
					selected = activeThreadList[mainSelectIndex].Tag
				}
				engine.vcs.configBeforeWrite()
				reply(selected)
			} else if ev.Rune() == 'o' {
				if os.Getenv("EDITOR") == "" {
					postError("EDITOR environment variable is not set.")
				}
				engine.vcs.configBeforeWrite()
				originate()
			} else if ev.Rune() == 'u' {
				if os.Getenv("EDITOR") == "" {
					postError("EDITOR environment variable is not set.")
				}
				selected := threadSelect
				if !inThread {
					selected = activeThreadList[mainSelectIndex].Tag
				}
				engine.vcs.configBeforeWrite()
				unsubscribe(selected)
			}
		}
	}
}

/* The HTML and JSON responder */

/*
 * Here is an inventory of all the attributes that can be in a request
 *
 * action: the main verb of the request.  Can be "repositories",
 * "project", "pending", "threadlist", "thread", or "sync".
 *
 * repo: The directory name of the repository relative to the
 * collection root.  A single path segment, doobles as an ID for the
 * repo rgat is unique to the site we are querying.
 *
 * filter: Defines a scope of messages to act on. Can be one of the
 * message classes "issues", "merge-requests", or "echoes", or the
 * universal selector "all", or it can be a thread ID.  In the future
 * (but not yet) it could be an individual message ID.
 *
 * kind: When the filter is a message ID, the class attribute is the
 * message class it belongs to.  This is a convenience feature, in principlw
 * the class can be deduced by looking at the selector.
 *
 * commit: Value is a message-ID. Can be passed back from a pending
 * or thread page to request that a locally queued message be
 * committed.
 *
 * discard: Value is a message-ID. Can be passed back from a pending
 * or thread page to request that a locally queued message be
 * discarded without committing.
 *
 * The following fields occur in project requests only:
 *
 * success: If nonempty, can be used as text to fill a status
 * area on the page so the result of a sync action doesn't just disappear.
 *
 * The following fields occur in submission requests only:
 *
 * tags: Contents of the tag field from the form.
 *
 * role: Can be "originate" or "reply".
 *
 * subject: Message subject line.
 *
 * inreplyto: ID of message selected as parent.
 *
 * text: The message text.
 *
 * markup: Submission requests only.  May be "literal", "markdown" or "asciidoc"
 *
 * visibility: can be "hide" or "reveal".
 *
 * oldtext: The thread and threadlist forms capture the entered text.  It is
 * available in this attribute when the page has to be refreshed due to an
 * input-validation error.
 *
 * assignedto: Value is a committer ID.
 *
 * subscribe: Can be "join" or "leave".
 *
 * merge: If presennt, submission describes a merge request.
 * The attribute is interpreted as an X-Merge_Request header.
 *
 * content-type: The MIME Content-Type header generated by issue and
 * MR submission requests, which become MIME-nultipart. We know this
 * won't collide with any enclosure content because the browser
 * generated it to avoid that.  Thus, we can use it when we generate
 * our internal representation of the message with attachments.
 *
 * The following fields occur in sync requests only:
 *
 * peer: the name of the upstream to sync to.
 */

const apiVersion = "0.0.0"

type wrapRepolist struct {
	Version  string
	Type     string
	Request  string
	Hostname string
	Result   []shimmerConfig
}

type wrapProject struct {
	Version        string
	Type           string
	Request        string
	Hostname       string
	Repository     shimmerConfig
	Remotes        []string
	HasReadmeName  bool
	ReadmeName     string
	ReadmeType     string
	ReadmeContents string
	Success        string `json:"-"`
	Error          string `json:"-"`
}

type wrapPending struct {
	Version    string
	Type       string
	Request    string
	Hostname   string
	Repository shimmerConfig
	Filter     string
	Result     []unpackedMessage
}

type wrapThreadlist struct {
	Version    string
	Type       string
	Request    string
	Hostname   string
	Repository shimmerConfig
	Filter     string
	Kind       string
	Visible    int
	Hidden     int
	Result     []datedThread
	OldText    string `json:"-"`
	Error      string `json:"-"`
	IsEcho     bool   `json:"-"`
	IsMerge    bool   `json:"-"`
}

type wrapThread struct {
	Version    string
	Type       string
	Request    string
	Hostname   string
	Repository shimmerConfig
	Committers []mail.Address
	Filter     string
	Kind       string
	Thread     *datedThread
	Result     []unpackedMessage
	OldText    string `json:"-"`
	Error      string `json:"-"`
}

type wrapError struct {
	Version    string
	Type       string
	Hostname   string
	Request    string
	Repository *shimmerConfig `json:",omitempty"`
	Result     string
}

// generate captures the logic of request performance,
// delegating actual output generation to four hook functions.
func generate(engine Engine, request string, fields map[string]string,
	repolistReport func(wrapRepolist),
	projectReport func(wrapProject),
	pendingReport func(wrapPending),
	threadlistReport func(wrapThreadlist),
	threadReport func(wrapThread),
	errorReport func(wrapError)) {
	log.Printf("%s -> %+v\n", request, fields)
	errout := func(leader string, ctx *shimmerConfig, err error) wrapError {
		if err != nil {
			leader += fmt.Sprintf(leader+": %s", ctx, err)
		}
		return wrapError{Version: apiVersion, Type: "Error", Request: request, Hostname: hostname, Repository: ctx, Result: leader}
	}
	for k, v := range map[string]string{"action": "repositories", "filter": "all", "format": "html"} {
		if fields[k] == "" {
			fields[k] = v
		}
	}
	if fields["action"] == "repositories" {
		repolist, err := engine.vcs.findNearbyRepositories()
		if err != nil {
			errorReport(errout("repository search error", nil, err))
		}

		wrapList := wrapRepolist{Type: "repolist",
			Version:  apiVersion,
			Request:  request,
			Hostname: hostname,
			Result:   make([]shimmerConfig, 0),
		}
		for _, config := range repolist {
			config.CloneURL = config.makeCloneURL()
			wrapList.Result = append(wrapList.Result, config)
		}
		// Because dictionary traverse order is random
		sort.Slice(wrapList.Result, func(i, j int) bool {
			return strings.Compare(wrapList.Result[i].Path, wrapList.Result[j].Path) == -1
		})
		repolistReport(wrapList)
		return
	}

	possibles, err := engine.vcs.findNearbyRepositories()
	if err != nil {
		errorReport(errout("repository search error", nil, err))
		return
	}

	config, ok := possibles[fields["repo"]]
	if !ok {
		errorReport(errout("no repository matches the request prefix", nil, nil))
		return
	}

	os.Chdir(fields["repo"])
	defer os.Chdir("..")

	state, err := engine.loadShimmerState(fields["filter"], fields["action"] == "thread" || fields["action"] == "pending")
	if err != nil {
		errorReport(errout("state load error", &config, err))
		return
	}

	if fields["action"] == "project" {
		under, err := ioutil.ReadDir("./")
		if err != nil {
			errorReport(errout("while looking for README", &config, err))
		}
		readmeName := ""
		readmeType := "literal"
		readmeContents := ""
		for _, f := range under {
			subname := f.Name()
			if readmeRE.MatchString(subname) {
				readmeName = subname
				break
			}
		}
		if readmeName != "" {
			for _, suffix := range []string{"md", "adoc"} {
				if strings.HasSuffix(readmeName, suffix) {
					readmeType = suffix
				}
			}
			if data, err := ioutil.ReadFile(readmeName); err == nil {
				readmeContents = string(data)
			}
		}
		config.CloneURL = config.makeCloneURL()
		wrapProject := wrapProject{
			Version:        apiVersion,
			Type:           "project",
			Request:        request,
			Hostname:       hostname,
			Repository:     config,
			Remotes:        engine.vcs.remotes(),
			HasReadmeName:  readmeName != "",
			ReadmeName:     readmeName,
			ReadmeType:     readmeType,
			ReadmeContents: readmeContents,
			Success:        fields["success"],
			Error:          fields["error"],
		}
		projectReport(wrapProject)
	} else if fields["action"] == "pending" {
		wrapResponse := wrapPending{
			Version:    apiVersion,
			Type:       "pending",
			Request:    request,
			Hostname:   hostname,
			Repository: config,
			Filter:     "pending",
			Result:     make([]unpackedMessage, 0),
		}
		for _, message := range state.messages {
			if !message.committed {
				wrapResponse.Result = append(wrapResponse.Result, unpack(message.mtext))
			}
		}
		//fmt.Printf("I see: %+v\n", wrapResponse)
		pendingReport(wrapResponse)
	} else if fields["action"] == "threadlist" {
		selector := fields["filter"]
		wrapResponse := wrapThreadlist{
			Version:    apiVersion,
			Type:       "threadlist",
			Request:    request,
			Hostname:   hostname,
			Repository: config,
			Filter:     selector,
			Kind:       selector,
			Result:     make([]datedThread, 0),
			OldText:    fields["text"],
			Error:      fields["error"],
			IsEcho:     selector == "echoes",
			IsMerge:    selector == "merge-requests",
		}
		for _, thread := range state.tagsByDate(true) {
			if tagSelectedBy(selector, thread.Tag) {
				wrapResponse.Result = append(wrapResponse.Result, thread)
				if thread.Visible {
					wrapResponse.Visible++
				} else {
					wrapResponse.Hidden++
				}
			}
		}
		threadlistReport(wrapResponse)
	} else if fields["action"] == "thread" {
		state.indexTags()
		selector := fields["filter"]
		kindOf := func(tag string) string {
			if strings.HasPrefix(tag, "issue:") {
				return "issues"
			}
			if strings.HasPrefix(tag, "merge-request:") {
				return "merge-request"
			}
			return "echoes"
		}
		thread := state.tagsbyname[selector]
		if thread == nil {
			errorReport(errout("invalid thread selector "+selector, &config, nil))
			return
		}
		// This ugly code ensures that if the thread has an
		// assignedto it is at the beginning of the list, so it
		// will be the default item in a generated select
		// menu.  Both parts of it assume that a committer ID
		// retrieved from Git is a valid email address.
		// The blank address, signifying no assignedto,
		// is always first or second.
		parsedAddresses := make([]mail.Address, 0)
		if thread.AssignedTo != nil {
			parsedAddresses = append(parsedAddresses, *thread.AssignedTo)
		}
		parsedAddresses = append(parsedAddresses, mail.Address{Name: "", Address: ""})
		for _, addr := range engine.vcs.getCommitters() {
			if pa, err := mail.ParseAddress(addr); err == nil {
				if thread.AssignedTo == nil || (pa.Name != thread.AssignedTo.Name && pa.Address != thread.AssignedTo.Address) {
					parsedAddresses = append(parsedAddresses, *pa)
				}
			}
		}

		wrapResponse := wrapThread{Type: "thread",
			Version:    apiVersion,
			Request:    request,
			Hostname:   hostname,
			Repository: config,
			Committers: parsedAddresses,
			Filter:     selector,
			Kind:       kindOf(selector),
			Thread:     thread,
			Result:     make([]unpackedMessage, 0),
			OldText:    fields["text"],
			Error:      fields["error"],
		}
		for _, message := range state.messages {
			if message.messageSelectedBy(selector) {
				unpacked := unpack(message.mtext)
				unpacked.Committed = message.committed
				// Ugh. It would be cleaner to pass through the map of
				// inherited headers here. but there's no way to make
				// that visible in the HTML template because the syntax
				// can't do map references.
				unpacked.NewSubject = unpacked.Subject != "" && !message.inherited["Subject"]
				unpacked.NewVisibility = unpacked.Visibility != "" && !message.inherited["X-Visibility"]
				unpacked.NewAssignedTo = unpacked.AssignedTo != nil && !message.inherited["X-Assigned-To"]
				unpacked.NewSubscription = unpacked.Subscription != "" && !message.inherited["X-Subscription"]
				unpacked.NewMergeRequest = unpacked.MergeRequest != "" && !message.inherited["X-Merge-Request"]
				wrapResponse.Result = append(wrapResponse.Result, unpacked)
			}
		}
		// Since the tags header is always present, we have to track actual changes.
		// We take the position here that the defiming message tag was awr just before
		// the start of ther thread.  The opposite chice would be defensuble, burtt this
		// is more convenient for displau generation.
		if len(wrapResponse.Result) > 0 {
			wrapResponse.Result[0].NewMessageTags = false
		}
		for i := 1; i < len(wrapResponse.Result); i++ {
			wrapResponse.Result[i].NewMessageTags = !wrapResponse.Result[i].MessageTags.Equal(wrapResponse.Result[i-1].MessageTags)
		}
		threadReport(wrapResponse)
	} else {
		errorReport(errout("unknown request type "+fields["action"], &config, err))
	}
}

func makeJSON(engine Engine, request string, args map[string]string, w io.Writer) {
	render := func(obj interface{}) {
		b, _ := json.MarshalIndent(obj, "", "    ")
		w.Write(b)
		w.Write([]byte{'\n'})
	}
	reportError := func(myerr wrapError) {
		render(myerr)
	}
	reportRepolist := func(repolist wrapRepolist) {
		render(repolist)
	}
	reportProject := func(project wrapProject) {
		render(project)
	}
	reportPending := func(pending wrapPending) {
		render(pending)
	}
	reportThreadlist := func(threadlist wrapThreadlist) {
		render(threadlist)
	}
	reportThread := func(thread wrapThread) {
		render(thread)
	}
	generate(engine, request, args,
		reportRepolist, reportProject, reportPending, reportThreadlist, reportThread, reportError)
}

// Declarative stuff for HTML

//Stylesheet swiped from https://ultimatemotherfuckingwebsite.com/
const stylesheetBlock = `
  <style>
            body{
                color: #333;
                background-color: #fdfdfe;
                font-size: 18px;
                font: 1.2rem/1.5 Century Gothic, sans-serif;
                max-width: 1200px;
                max-width: 110ch;
                margin: 0 auto;
                padding: 2.5rem 1.75rem;
            }
            h1,h2,h3,h4,h5,h6{
                line-height: 1.2; 
                font-family: Arial, serif;
            }
            h1{
                font-size: 2.45rem;
            }
            a:focus{
                outline: 0.2rem solid #123;
                outline-offset: 0.4rem;
            }
            p{
                margin-bottom: 2rem;
            }
            section, aside, footer{
                margin: 2.5rem auto;
            }
            ul{
                margin-top: -1.25rem;
                margin-bottom: 2.25rem;
            }
            b{
                background-color: rgba(200,195,190,0.15);
                padding: 0.4rem;
                margin: -0.4rem;
            }
            dt{
                font-weight: bold;
                margin-top: 1rem; 
            }
            dt:target{
                background-color: rgba(200,195,190,0.35);
                padding: 0.2rem;
                margin: 0.8rem -0.2rem 0 -0.2rem;
                outline: 2px solid rgba(125,125,125, 0.75);
            }
	    // What to do on a device that prefers a dark color scheme
            @media (prefers-color-scheme: dark){
                body{color:#ddd;background:#123}
                a:focus{
                    outline: 0.2rem solid #fed;
                }
                a{color:#5bf;}
            }
	    textarea {
		width: 100%;
		height: 150px;
		padding: 12px 20px;
		box-sizing: border-box;
		border: 2px solid #ccc;
		border-radius: 4px;
		background-color: #f8f8f8;
		/*resize: none;*/
	    }
	    input[type=text] {
	        width: 100%;
	    }
            input[type=button], input[type=submit], input[type=reset] {
                background-color: #87CEEB;
                border: none;
                color: black;
                padding: 16px 32px;
                text-decoration: none;
                margin: 4px 2px;
                cursor: pointer;
            }
	    /* From https://every-layout.dev/rudiments/boxes/ */ 
	    * {
	      box-sizing: border-box;
	    }
	    .navlink a:link {color:black;text-decoration:none;}
	    .navlink a:visited {color:black;text-decoration:none;}
	    .navlink a:hover {text-decoration:underline;}
	    .navlink a:active {color:black;text-decoration:underline;}
	    /* https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link */
	    a.linkbutton {
		-webkit-appearance: button;
		-moz-appearance: button;
		appearance: button;

		text-decoration: none;
		background-color: #87CEEB; 
		border: 0px;
		color: black;
		cursor: pointer; /* Pointer/hand icon */
	    }
  </style>
`

var errorTemplateString = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Error response</title>
  <meta name="description" content="Error detected while processing your request">
` + stylesheetBlock + `
</head>
<body>
  <main>
    <p>Request {{.Request}} produced an error: {{.Result}}</p>
  </main>
</body>
</html>
`

var repolistTemplateString string = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>List of repositories hosted at {{.Hostname}}</title>
  <meta name="description" content="List of repositories hosted at this Shimmer site">
` + stylesheetBlock + `
</head>
<body>
  <header>
     <strong>{{.Hostname}}</strong> <span style='float:right'>{{$count := len .Result}}{{pluralize $count "repository" "repositories"}}</span>
     <hr/>
  </header>  
  <main>
      {{range .Result}}<p><a href="/shimmer?repo={{.Path}}&action=project">{{.Path}}: {{.Description}}</a></p>{{end}}
  </main>
</body>
</html>
`

var projectTemplateString string = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{.Repository.Path}} landing page</title>
  <meta name="description" content="Project information about {{.Repository.Path}}">
` + stylesheetBlock + `
</head>
<body>
  <header>
     <nav class="navlink"><strong><a href="/shimmer?action=repositories">{{.Hostname}}</a></strong> &rArr; <strong>{{.Repository.Path}}</strong></nav>
     <hr/>
  </header>  
  <main style="display:flex">
    <div class="navlink" style="padding:1rem; background-color:#eee; border:1px dashed #999;">
      <a href="/shimmer?repo={{.Repository.Path}}&action=pending">Pending</a><br/>
      <a href="/shimmer?repo={{.Repository.Path}}&action=threadlist&filter=issues">Issues</a><br/>
      <a href="/shimmer?repo={{.Repository.Path}}&action=threadlist&filter=merge-requests">Merge Requests</a><br/>
      <a href="/shimmer?repo={{.Repository.Path}}&action=threadlist&filter=echoes">Echoes</a><br/>
   </div>
   <div style="padding:1rem;display:block;">
     {{if .Repository.Description}}<div>{{.Repository.Description}}{{else}}Repository has no description.</div>{{end}}
     <div>Clone this repository with <strong>git clone {{.Repository.CloneURL}}</strong></div>
     {{if .Remotes}}
	<div>
	{{$repo := .Repository.Path}}
	<hr/>
	<table border=1 width=30%>
	  <caption>Remotes</caption>
	{{range .Remotes}}
	  <tr><td>{{.}}</td><td><a href="/shimmer?action=sync&repo={{$repo}}&peer={{.}}" class="linkbutton">Sync</a></td></tr>
	{{end}}
	</table>
	</div>
      {{else}}
	 <div>No remotes have been set up for this repository.</div>
      {{end}}
    </div>
    {{if .Error}}<p style='color:red'>{{.Error}}</p>{{end}}
    {{if .Success}}<p style='color:green'>{{.Success}}</p>{{end}}
  </main>
  {{if .HasReadmeName}}<hr/><p><strong>{{.ReadmeName}}:</strong><p>
  <div>{{renderText .ReadmeContents .ReadmeType}}</div>{{end}}
</body>
</html>
`

var threadlistTemplateString string = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Index of threads selected by {{.Repository.Path}} and {{.Filter}}</title>
  <meta name="description" content="Index of threads selected by {{.Repository.Path}} and {{.Filter}}">
` + stylesheetBlock + `
</head>
<body>
  <header>
     <nav class="navlink"><strong><a href="/shimmer?action=repositories">{{.Hostname}}</a> &rArr; <a href="/shimmer?repo={{.Repository.Path}}&action=project">{{.Repository.Path}}</a> &rArr; {{.Filter}}</strong> <span style='float:right'>Total: {{len .Result}}  Visible: {{.Visible}}  Hidden: {{.Hidden }}</span></nav>
     <hr/>
  </header>{{$path := .Repository.Path}}
  <main>{{range .Result}}{{if .Visible}}
    <table width='100%' style='outline: 2px solid #ddd;'>
      <tr>
        <td align='left'><strong><a href='/shimmer?repo={{$path}}&action=thread&filter={{.Tag}}'>{{.Subject}}</a></strong></td>
        <td align='right'>{{if .Visible}}Open{{else}}Closed{{end}} - {{if .IsEcho}}{{pluralize .Count "message" "messages"}}{{else}}{{pluralize .Count "comment" "comments"}}{{end}}</td>
      </tr>
      <tr>
        <td align='left'>{{if .IsEcho}}<small>{{.Tag}}</small> - {{end}}created {{relativeTime .Began}} by {{.Originator.Name}}</td>
        <td align='right'>updated {{relativeTime .Updated}} ago</td>
      </tr>
    </table>
    {{end}}{{end}}</dl>
  </main>
    <div>
      <div style="padding:1rem"></div>
      <form action="/shimmer/originate" enctype="multipart/form-data" method="POST">
      <span><strong>Create a new {{.Filter}} thread:</strong></span><br/>
      {{if .Error}}<span style='color:red'>{{.Error}}</span><br/>{{end}}
      <label>Who are you? <input type="text" name="whoami"/></label><br/>
      <label>Subject: <input type="text" name="subject"/></label><br/>
      {{if .IsMerge}}<label>MergeRequest: <input type="text" name="merge"/></label><br/>{{end}}
      <label>Additional tags (optional): <input type="text" name="tags"/></label><br/>
      <textarea rows="10" name="text" placeholder="Enter your message here.">{{.OldText}}</textarea><br/>
      <input type="hidden" name="action" value="threadlist"/>
      <input type="hidden" name="hostname" value="{{.Hostname}}"/>
      <input type="hidden" name="repo" value="{{.Repository.Path}}"/>
      <input type="hidden" name="filter" value="{{.Filter}}"/>
      <input type="hidden" name="oldtext" value="{{.OldText}}" value="{{.OldText}}"/>
      Interpret as:
      <input type="radio" name="markup" value="literal" checked> Literal
      <input type="radio" name="markup" value="markdown"> Markdown
      <input type="radio" name="markup" value="asciidoc"> Asciidoc<br/>
      <input type="submit" name="submit" value="Submit"/>
      </form>
    </div>
</body>
</html>
`

var pendingTemplateString string = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pending traffic at {{.Hostname}}/{{.Repository.Path}}</title>
  <meta name="description" content="Pendig traffic at {{.Hostname}}/{{.Repository.Path}}">
` + stylesheetBlock + `
</head>
<body>
  <header>
  <nav class="navlink"><strong><a href="/shimmer?action=repositories">{{.Hostname}}</a> &rArr; <a href="/shimmer?repo={{.Repository.Path}}&action=project">{{.Repository.Path}}</a> &rArr; {{.Filter}}</strong> <span style='float:right'>{{$count := len .Result}}{{pluralize $count "message" "messages"}}</span></nav>
  <hr/>
  </header>
  <main>
    {{$outer := .}}{{range $msgcount, $element := .Result}}
    <div style="display:flex;">
      <div>{{$element.From.Name}} {{relativeTime $element.Date}} ago</div>
      <div style="margin-left:auto">
        <a href="/shimmer?action=pending&repo={{$outer.Repository.Path}}&commit={{$element.MessageID}}" class="linkbutton">Commit</a>
        <a href="/shimmer?action=pending&repo={{$outer.Repository.Path}}&discard={{$element.MessageID}}" class="linkbutton">Discard</a>
      </div>
    </div>
    Thread topic: "{{$element.Subject}}"<br/>
    Tags: {{$element.MessageTags}}<br/>
    {{if $element.MergeRequest}}Merge Request: {{$element.MergeRequest}}<br/>{{end}}
    {{if $element.Visibility}}Thread visibility: "{{$element.Visibility}}".<br/>{{end}}
    {{if $element.AssignedTo}}Assignee to "{{$element.AssignedTo.Name }}".<br/>{{end}}
    {{if $element.Subscription}}Subscription action "{{$element.Subscription}}".<br/>{{end}}
    {{if $element.Body }}<div style="outline: 2px solid #ddd">
      <p>{{renderText $element.Body $element.Markup}}</p>
    </div>{{end}}
    {{end}}
    {{if .Error}}<p style='color:red'>{{.Error}}</p>{{end}}
    </main>
</body>
</html>
`

var threadTemplateString string = `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Thread: {{.Thread.Subject}}</title>
  <meta name="description" content="{{.Thread.Subject}}">
` + stylesheetBlock + `
</head>
<body>
  <header>
  <nav class="navlink"><strong><a href="/shimmer?action=repositories">{{.Hostname}}</a> &rArr; <a href="/shimmer?repo={{.Repository.Path}}&action=project">{{.Repository.Path}}</a> &rArr; <a href="/shimmer?action=threadlist&repo={{.Repository.Path}}&filter={{.Kind}}">{{.Kind}}</a> &rArr; {{.Filter}}</strong> <span style='float:right'>{{if .Thread.Visible}}Open{{else}}Closed{{end}} - {{$count := len .Result}}{{pluralize $count "comment" "comments"}}</span></nav>
  <hr/>
  </header>
  <main>
    <div style="display:flex">
      <div><strong>{{.Thread.Subject}}</strong></div>
      <div style="margin-left:auto">{{if not .Thread.Visible}}Hidden - {{end}} created {{relativeTime .Thread.Began}} by {{.Thread.Originator.Name}}</div>
    </div>
    <div style="padding:1rem"></div>
    {{$outer := .}}{{range $msgcount, $element := .Result}}
    <div style="outline: 2px solid #ddd;">
      <div style="display:flex;">
	<div>{{$element.From.Name}} {{relativeTime $element.Date}} ago</div>
	  {{if not $element.Committed}}<div style="margin-left:auto">
	    <a href="/shimmer?action=thread&repo={{$outer.Repository.Path}}&filter={{$outer.Filter}}&commit={{$element.MessageID}}" class="linkbutton">Commit</a>
	    <a href="/shimmer?action=thread&repo={{$outer.Repository.Path}}&filter={{$outer.Filter}}&discard={{$element.MessageID}}" class="linkbutton">Discard</a>
	  </div>{{end}}
	<div style="margin-left: auto;"><input for="reply" type="radio" name="inreplyto" value="{{$element.MessageID}}"> Select as reply parent</div>
      </div>
      {{if and $element.NewSubject $msgcount}}Set the thread topic to: "{{$element.Subject}}"<br/>{{end}}
      {{if and $element.NewMessageTags $msgcount}}Set the thread tags to: {{$element.MessageTags}}<br/>{{end}}
      {{if $element.NewMergeRequest}}Requested a merge: {{$element.MergeRequest}}<br/>{{end}}
      {{if $element.NewVisibility}}Changed the thread visibility to "{{$element.Visibility}}".<br/>{{end}}
      {{if $element.NewAssignedTo}}Changed the assignee to "{{$element.AssignedTo.Name }}".<br/>{{end}}
      {{if $element.NewSubscription}}Took the subscription action "{{$element.Subscription}}".<br/>{{end}}
      {{if $element.Body }}<div>{{renderText $element.Body $element.Markup}}</div>
      {{if .Attachments}}<div style="display:flex;">
	<div style="padding:1rem;">{{$acount := len .Attachments}}{{pluralize $acount "Attachment" "Attachments"}}:</div>
	{{range .Attachments}}<div style="padding:1rem;"><a href="data:text/plain;base64,{{.Content}}" download="{{.Filename}}">{{.Filename}}</a></div>{{end}}
	</div>{{end}}
      {{end}}
    </div>
    {{end}}
    <div>
      <div style="padding:1rem"></div>
      <span><strong>Reply to this {{.Kind}} thread:</strong></span><br/>
      {{if .Error}}<p style='color:red'>{{.Error}}</p>{{end}}
      <form id="reply" action="/shimmer/reply" enctype="multipart/form-data" method="POST">
	<label>Who are you? <input type="text" name="whoami"/></label><br/>
	<label>Subject: <input type="text" name="subject" value="{{.Thread.Subject}}"/></label><br/>
	{{if .Thread.MergeRequest}}<label>Merge Request: <input type="text" name="merge" value="{{.Thread.MergeRequest}}"/></label><br/>{{end}}
	<label>Additional tags (optional): <input type="text" name="tags"/></label><br/>
	<textarea rows="10" name="text" placeholder="Enter your message here.">{{.OldText}}</textarea><br/>
	<input type="hidden" name="action" value="thread"/>
	<input type="hidden" name="hostname" value="{{.Hostname}}"/>
	<input type="hidden" name="repo" value="{{.Repository.Path}}"/>
	<input type="hidden" name="kind" value="{{.Kind}}"/>
	<input type="hidden" name="filter" value="{{.Filter}}"/>
	<input type="hidden" name="oldsubject" value="{{.Thread.Subject}}"/>
	<input type="hidden" name="oldmerge" value="{{.Thread.MergeRequest}}"/>
	<input type="hidden" name="oldtext" value="{{.OldText}}"/>
	<input type="hidden" name="oldvisibility" value="{{.Thread.Visible}}"/>
	<input type="hidden" name="oldassignedto" value="{{.Thread.AssignedTo}}"/>
	Interpret as:
	<input type="radio" name="markup" value="literal" checked> Literal
	<input type="radio" name="markup" value="markdown"> Markdown
	<input type="radio" name="markup" value="asciidoc"> Asciidoc<br/>
	<table width="100%" style="text-align:center;"><tr>
	  {{if not .Thread.IsEcho}}<td>
	    <input id=visibility type="checkbox" name="visibility" value="{{if .Thread.Visible}}hide{{else}}Reveal{{end}}"><label for=visibility>{{if .Thread.Visible}}Close{{else}}Reopen{{end}} thread</label>
	  </td>{{end}}
	  {{if not .Thread.IsEcho}}<td>
	    <label for="assignedto">Assigned To:</label>
	    <select id="assignedto" name="assignedto" size=1 multiple>
	      {{range .Committers}}<option value="{{.Name}}">{{if .Name}}{{.Name}}{{else}}Nobody{{end}}</option>{{end}}
	    </select>
	  </td>{{end}}
	  <td>
	    <!-- Ugh. Ideally, we'd know user's subscription status and present a toggle here -->
	    <label> Subscribe: <input type="radio" name="subscribe" value="join"></label>
	    <label> Unsubscribe: <input type="radio" name="subscribe" value="leave"></label>
	  </td>
	  <td>
	    <label>Attach a file: <input type="file" id="attachment" name="attachment"></label>
	  </td>
	</tr></table>
	<input type="submit" name="submit" value="Submit"/>
      </form>
    </div>
    </main>
</body>
</html>
`

func relativeTime(t time.Time) time.Duration {
	if clockbase > 0 {
		return time.Duration(0)
	}
	return time.Since(t).Truncate(1 * time.Minute)
}

func renderText(rawtext string, markup string) template.HTML {
	if markup == "asciidoc" || markup == "ad" {
		buf := new(bytes.Buffer)
		_, err := asciidoc.Convert(strings.NewReader(rawtext), buf, mconfig.NewConfiguration())
		if err == nil {
			return template.HTML(buf.String())
		}
		// On error, fall through to rendering as plain text
	}
	if markup == "markdown" || markup == "md" {
		return template.HTML(string(markdown.ToHTML([]byte(rawtext), nil, markdownRenderer)))
	}
	// If we're plain-rendering, remove the message's trailing
	// spacer line. It might not have one if it has been pulled
	// from the local moderation queue.
	if len(rawtext) > 0 && rawtext[len(rawtext)-1] == '\n' {
		rawtext = rawtext[:len(rawtext)-1]
	}
	return template.HTML("<pre>" + template.HTMLEscapeString(rawtext) + "</pre>")
}

func pluralize(count int, singular, plural string) string {
	noun := singular
	if count != 1 {
		noun = plural
	}
	return fmt.Sprintf("%d %s", count, noun)
}

var errorTemplate *template.Template
var repolistTemplate *template.Template
var projectTemplate *template.Template
var pendingTemplate *template.Template
var threadlistTemplate *template.Template
var threadTemplate *template.Template

var markdownRenderer markdown.Renderer
var readmeRE *regexp.Regexp

func init() {
	errorTemplate = template.Must(template.New("errorTemplate").Parse(errorTemplateString))
	repolistTemplate = template.Must(template.New("repolistTemplate").Funcs(template.FuncMap{"relativeTime": relativeTime, "pluralize": pluralize}).Parse(repolistTemplateString))
	projectTemplate = template.Must(template.New("projectTemplate").Funcs(template.FuncMap{"renderText": renderText}).Parse(projectTemplateString))
	pendingTemplate = template.Must(template.New("pendingTemplate").Funcs(template.FuncMap{"relativeTime": relativeTime, "renderText": renderText, "pluralize": pluralize}).Parse(pendingTemplateString))
	threadlistTemplate = template.Must(template.New("threadlistTemplate").Funcs(template.FuncMap{"relativeTime": relativeTime, "pluralize": pluralize}).Parse(threadlistTemplateString))
	threadTemplate = template.Must(template.New("threadTemplate").Funcs(template.FuncMap{"relativeTime": relativeTime, "renderText": renderText, "pluralize": pluralize}).Parse(threadTemplateString))

	htmlFlags := mhtml.CommonFlags | mhtml.HrefTargetBlank
	opts := mhtml.RendererOptions{Flags: htmlFlags}
	markdownRenderer = mhtml.NewRenderer(opts)
	readmeRE = regexp.MustCompile(`(?i)read[.]*me`)
}

func makeHTML(engine Engine, request string, args map[string]string, w io.Writer) {
	reportError := func(myerr wrapError) {
		errorTemplate.ExecuteTemplate(w, "errorTemplate", myerr)
	}
	reportRepolist := func(repolist wrapRepolist) {
		repolistTemplate.ExecuteTemplate(w, "repolistTemplate", repolist)
	}
	reportProject := func(project wrapProject) {
		projectTemplate.ExecuteTemplate(w, "projectTemplate", project)
	}
	reportPending := func(pending wrapPending) {
		pendingTemplate.ExecuteTemplate(w, "pendingTemplate", pending)
	}
	reportThreadlist := func(threadlist wrapThreadlist) {
		threadlistTemplate.ExecuteTemplate(w, "threadlistTemplate", threadlist)
	}
	reportThread := func(thread wrapThread) {
		threadTemplate.ExecuteTemplate(w, "threadTemplate", thread)
	}
	generate(engine, request, args,
		reportRepolist,
		reportProject,
		reportPending,
		reportThreadlist,
		reportThread,
		reportError)
}

/* Gather together the output modes */

func report(engine Engine, request string, args map[string]string, w io.Writer) {
	if args["format"] == "json" {
		makeJSON(engine, request, args, w)
	} else {
		makeHTML(engine, request, args, w)
	}
}

/* The server */

const maxAttachmentSize = 4194304

// This is a list of file extensions that are common
// worm vectors on Windows systems. By rejecting these
// we'll foil a lot of automated spamming attemps.
var spamExtensions = []string{
	".bat",
	".chm",
	".cmd",
	".com",
	".cpl",
	".crt",
	".exe",
	".hlp",
	".hta",
	".inf",
	".ins",
	".isp",
	".jse",
	".lnk",
	".mdb",
	".pcd",
	".pif",
	".reg",
	".scr",
	".sct",
	".shs",
	".vb",
	".ws",
	".zip",
}

/* submit accepts, validates, and processes all requestrs to append to the message queue */
func submit(engine Engine, request map[string]string, form *multipart.Form) map[string]string {
	repolist, err := engine.vcs.findNearbyRepositories()
	if err != nil {
		request["error"] = fmt.Sprintf("repository search error: %s", err)
		return request
	}
	relpath := request["repo"]
	if _, ok := repolist[relpath]; !ok {
		request["error"] = fmt.Sprintf("can't find a repository named %q", relpath)
		delete(request, "role")
		return request
	}

	defer delete(request, "role")

	os.Chdir(relpath)
	defer os.Chdir("..")

	if _, err := mail.ParseAddress(request["whoami"]); err != nil {
		request["error"] = fmt.Sprintf("invalid submitter address: %s", err)
		return request
	}

	if request["role"] == "originate" && request["subject"] == "" {
		request["error"] = fmt.Sprintf("A new thread requires a nonempty subject line.")
		return request
	}

	message := ""

	// Replies always have the tag of their thread
	if request["role"] == "reply" {
		request["tags"] = strings.TrimSpace(request["filter"] + " " + request["tags"])
	}

	// Things possibly supplied by the web interface
	changes := 0
	for _, item := range [][3]string{
		{"oldsubject", "subject", "Subject"},
		{"oldmerge", "merge", "X-Merge-Request"},
		{"", "inreplyto", "In-Reply-To"},
		{"", "tags", "X-Message-Tags"},
		{"", "markup", "X-Markup"},
		{"oldvisibility", "visibility", "X-Visibility"},
		{"oldassignedto", "assignedto", "X-Assigned-To"},
		{"", "subscribe", "X-Subscription"},
	} {
		oldtag := item[0]
		newtag := item[1]
		hdrname := item[2]

		if request[newtag] != "" && (oldtag == "" || request[newtag] != request[oldtag]) {
			message += fmt.Sprintf("%s: %s\n", hdrname, request[newtag])
			changes++
		}
	}

	if changes == 0 {
		request["error"] = fmt.Sprintf("Nothing new submitted.")
		return request
	}

	var boundary string
	if len(form.File) > 0 {
		_, params, err := mime.ParseMediaType(request["Content-Type"])
		if err != nil {
			request["error"] = fmt.Sprintf("Ill-formed MIME content header.")
			return request
		}
		message += fmt.Sprintf("MIME-Version: 1.0\nContent-Type: multipart/mixed; boundary=%s\n", params["boundary"])
	}

	if len(message) > 0 {
		message += "\n"
	}
	if len(form.File) == 0 {
		// Simple case, plain message
		message += request["text"]
	} else {
		// Form with plain text followed by attachments
		var b bytes.Buffer
		w := multipart.NewWriter(&b)
		w.SetBoundary(boundary)

		// Write the message part as text/plain
		partHeaders := textproto.MIMEHeader{}
		partHeaders.Set("Content-Type", "text/plain")
		part, err := w.CreatePart(partHeaders)
		if err != nil {
			request["error"] = fmt.Sprintf("MIME part creation failed: %s", err)
			return request
		}
		if _, err = part.Write([]byte(request["text"])); err != nil {
			request["error"] = fmt.Sprintf("MIME part write failedx: %s", err)
			return request
		}
		// I don't know why the File member of multipart.Form is a dictionary,
		// nor what the keys are. Fortunately we don't care, we just
		// want to extract the file names and file data.
		var totalsize int64
		for _, attachmentGroup := range form.File {
			for _, attachment := range attachmentGroup {
				// Antispam filter.  Robots looking to propagate worms often do it with
				// a bogus enclosure that they expect to be auto-opened and run on a
				// Windows system.  Prevent this by rejecting spammy enclosure types -
				// easy call because there's no legitimate role for these in Unix-land.
				for _, ext := range spamExtensions {
					if strings.HasSuffix(attachment.Filename, ext) {
						request["error"] = fmt.Sprintf("%s enclosures are not accepted here", ext)
						return request
					}
				}
				partHeaders := textproto.MIMEHeader{}
				partHeaders.Set("Content-Type", "binary")
				partHeaders.Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", attachment.Filename))
				part, err := w.CreatePart(partHeaders)
				if err != nil {
					request["error"] = fmt.Sprintf("MIME attachment part creation failed: %s", err)
					return request
				}
				f, err := attachment.Open()
				if err != nil {
					request["error"] = fmt.Sprintf("MIME attachment open of %s failed: %s", attachment.Filename, err)
					return request
				}
				defer f.Close()
				// Policy choice: we just copy the raw attachment bytes here.
				// We could base64 encode, and that would be a better plan if
				// we were going to push these in email notifications.  But (a)
				// we expect most attachments to be textual, and (b) we're saving
				// to an 8-bit-clean medium :-).  So we go for the choice that
				// minimaizes storage costs and makes text attachments readable
				// in situ.
				partlen, err := io.Copy(part, f)
				if err != nil {
					request["error"] = fmt.Sprintf("MIME attachment part copy failed: %s", err)
					return request
				}
				totalsize += partlen
				if totalsize > maxAttachmentSize {
					request["error"] = fmt.Sprintf("Attachment rejected, too large")
					return request
				}
			}
		}
		w.Close()
		message += b.String()
		form.RemoveAll()
	}

	parsedUpdate, err := engine.requestParser(strings.NewReader(message), "", request["whoami"])
	if err != nil {
		request["error"] = fmt.Sprintf("Message parse of %q failed: %s", message, err)
		return request
	}
	if err = engine.enqueue(parsedUpdate); err != nil {
		request["error"] = fmt.Sprintf("Message submission failed: %s", err)
		return request
	}

	return request
}

/* commit accepts, validates, and processes all requests to commit to or discard from the message queue */
func dequeue(engine Engine, request map[string]string) map[string]string {
	var discard bool

	if request["action"] == "sync" {
		repolist, err := engine.vcs.findNearbyRepositories()
		if err != nil {
			error := fmt.Sprintf("repository search error: %s", err)
			return map[string]string{"action": "project", "repo": request["repo"], "error": error}
		}
		relpath := request["repo"]
		if _, ok := repolist[relpath]; !ok {
			error := fmt.Sprintf("can't find a repository named %q", relpath)
			return map[string]string{"action": "project", "repo": request["repo"], "error": error}
		}

		os.Chdir(relpath)
		defer os.Chdir("..")

		_, err = engine.vcs.sync(request["peer"])
		if err == nil {
			success := fmt.Sprintf("Sync to %s succeeded.", request["peer"])
			return map[string]string{"action": "project", "repo": request["repo"], "success": success}
		}

		error := fmt.Sprintf("Sync to %s failed: %s", request["peer"], err)
		return map[string]string{"action": "project", "repo": request["repo"], "error": error}
	}

	// Commit and discard requests
	commitID, ok := request["commit"]
	if ok {
		discard = false
	} else if commitID, ok = request["discard"]; ok {
		discard = true
	}
	delete(request, "commit")
	if commitID == "" {
		return request
	}

	repolist, err := engine.vcs.findNearbyRepositories()
	if err != nil {
		request["error"] = fmt.Sprintf("repository search error: %s", err)
		return request
	}
	relpath := request["repo"]
	if _, ok := repolist[relpath]; !ok {
		request["error"] = fmt.Sprintf("can't find a repository named %q", relpath)
		return request
	}

	os.Chdir(relpath)
	defer os.Chdir("..")

	path := filepath.Join(engine.vcs.repoDirectory(), "shimmer", "spool", commitID)
	fp, err := os.Open(path)
	if err != nil {
		abs, _ := filepath.Abs(path)
		request["error"] = fmt.Sprintf("queue retrieval open failed for %s: %s", abs, err)
		return request
	}

	b, err := ioutil.ReadAll(fp)
	if err != nil {
		abs, _ := filepath.Abs(path)
		request["error"] = fmt.Sprintf("queue retrieval read failed for %s: %s", abs, err)
		return request
	}

	if !discard {
		engine.vcs.commitNote(string(b))
		if !nomail && engine.vcs.parseConfig(".").NotifyEnable {
			if p := unpack(string(b)); p.Error == "" {
				// Unavoidably, this does another state read.
				pushNotify(string(b), engine.subscriptionList(strings.Join(p.MessageTags, ",")))
			}
		}
	}
	os.Remove(path)

	return request
}

func serve(engine Engine, args []string) {
	addrport := ":3033"
	if len(args) < 0 {
		addrport = args[0]
	}

	log.Printf("server launching, browse https://localhost%s/shimmer ", addrport)

	genericGetHandler := func(w http.ResponseWriter, r *http.Request) {
		errout := func(format string, args ...interface{}) {
			fmt.Fprintf(w, "<p>"+format+"</p>\n", args...)
		}
		query, err := url.ParseQuery(r.URL.RawQuery)
		if err != nil {
			errout("Ill-formed query string: %s", r.URL.RawQuery)
			return
		}

		reduced := make(map[string]string)
		for k := range query {
			reduced[k] = query[k][0]
		}

		reduced = dequeue(engine, reduced)
		report(engine, r.URL.Path, reduced, w)
	}

	genericFormHandler := func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseMultipartForm(maxAttachmentSize); err != nil {
			log.Printf("ill-formed form request")
			return
		}

		reduced := make(map[string]string)
		for k, v := range r.MultipartForm.Value {
			reduced[k] = v[0]
		}

		// The browser computed a suitable MIME content header
		// in order to send the request with the attachment
		// embedded; use that rather than computing our own.
		// In a Chrome-derived brower typically looks like this:
		// "multipart/form-data; boundary=----WebKitFormBoundaryNq1wiSBPRfE1fA1"
		// however it would be unwise to assome the boundary string is
		// newver quoted by other browsers.
		reduced["content-type"] = r.Header.Get("Content-Type")

		if strings.HasSuffix(r.URL.Path, "originate") {
			reduced["role"] = "originate"
		} else if strings.HasSuffix(r.URL.Path, "reply") {
			reduced["role"] = "reply"
		}
		reduced = submit(engine, reduced, r.MultipartForm)
		report(engine, r.URL.Path, reduced, w)
	}

	http.HandleFunc("/shimmer", genericGetHandler)
	http.HandleFunc("/shimmer/originate", genericFormHandler)
	http.HandleFunc("/shimmer/reply", genericFormHandler)

	log.Fatal(http.ListenAndServe(addrport, nil))
}

/* The main event */

func main() {
	flag.StringVar(&repodir, "c", ".", "where repositories are hosted (default is .)")
	flag.BoolVar(&nomail, "n", false, "suppress notification sends")
	flag.UintVar(&verbose, "v", 0, "set verbosity level")
	flag.Int64Var(&clockbase, "T", 0, "clock base for developer test mode")
	flag.Parse()

	var err error
	repodir, err = filepath.Abs(repodir)
	if err != nil {
		croak("path lookup failure: %s", err)
	}
	if !exists(repodir) || !isdir(repodir) {
		croak("repository directory %s is nonexistent or not a directory", repodir)
	}
	os.Chdir(repodir)

	var command string
	var arguments []string

	if flag.NArg() == 0 {
		command = "tui"
		arguments = make([]string, 0)
	} else {
		command = flag.Arg(0)
		arguments = flag.Args()[1:]
	}

	hostname, err = fqdn.FqdnHostname()
	if err != nil {
		croak("hostname lookup failure: %s", err)
	}

	var engine Engine
	engine.vcs = newVCS()

	sanecheck := func() {
		if !engine.vcs.isRepository(".") {
			d, _ := os.Getwd()
			croak("current directory %s doesn't look like a Git repository.", d)
		}

		if !engine.vcs.hasBranches() {
			croak("can't operate on an empty repository")
		}
	}

	threadFilter := strings.TrimSpace(strings.Join(arguments, " "))

	switch command {
	case "compose":
		fallthrough
	case "co":
		sanecheck()
		engine.vcs.configBeforeWrite()
		if p, err := edit(&engine, nil, "", "Subject: \n\n"); err != nil {
			croak("compose failed: %s", err)
		} else {
			if p.threadTags != "" {
				os.Stdout.WriteString(p.threadTags + "\n")
			}
			if !nomail && engine.vcs.parseConfig(".").NotifyEnable {
				pushNotify(p.dump(), engine.subscriptionList(p.threadTags))
			}
		}
	case "clone":
		var local string
		if len(arguments) < 1 {
			croak("clone failed - requires one or two arguments")
		}
		upstream := arguments[0]
		if len(arguments) == 1 {
			local = filepath.Base(upstream)
		} else {
			local = arguments[1]
		}
		if err := engine.vcs.clone(upstream, local); err != nil {
			croak("clone failed: %s", err)
		}
	case "sync":
		sanecheck()
		var upstream = "origin"
		if len(arguments) > 0 {
			upstream = arguments[0]
		}
		if !newOrderedStringSet(engine.vcs.remotes()...).Contains(upstream) {
			croak("no such remote as %s", upstream)
		}
		if _, err = engine.vcs.sync(upstream); err != nil {
			croak("sync failure: %s", err)
		}
	case "cat":
		sanecheck()
		engine.cat(threadFilter, os.Stdout)
	case "mbox":
		sanecheck()
		engine.mbox(threadFilter, os.Stdout)
	case "list":
		sanecheck()
		if state, err := engine.loadShimmerState(threadFilter, false); err == nil {
			for _, thread := range state.tagsByDate(true) {
				os.Stdout.WriteString(thread.Tag + "\n")
			}
		} else {
			croak("list state load error: %s", err)
		}
	case "tui":
		sanecheck()
		tui(&engine, threadFilter)
	case "getconfig":
		sanecheck()
		engine.vcs.getConfig(".", os.Stdout)
	case "setconfig":
		sanecheck()
		data := new(bytes.Buffer)
		_, err = io.Copy(data, os.Stdin)
		if err != nil {
			croak("setConfig unput copy: %s", err)
		}
		if err = engine.vcs.setConfig(engine.vcs.userid(), data.String(), "Shimmer configuration change"); err != nil {
			croak("configuration setting failed: %s", err)
		}

	// Past this point the commends no longer use sanecheck;
	// the commands don't assume we're actually in a repository
	// but rather in a directory containing a repository collection.
	// FIXME: implement servertest

	case "serve":
		serve(engine, arguments)
	case "query": // Undocumented - for testing
		qd := make(map[string]string)
		for _, item := range strings.Split(arguments[0], "&") {
			fields := strings.Split(item, "=")
			if len(fields) != 2 {
				croak("ill-formed query")
			}
			qd[fields[0]] = fields[1]
		}
		report(engine, arguments[0], qd, os.Stdout)

	// These commands don't need to be in a repository directory
	// or superdirectory at all
	/*
		case "makekeys":
			public, private, err := minisign.GenerateKey(rand.Reader)
			if err != nil {
				log.Fatalln(err)
			}
	*/
	case "help":
		os.Stdout.WriteString("shimmer [options] [help | co[mpose] |cat [filter...] | list [filter...] | tui [filter...] | serve [addr:port] ]\n\n")
		flag.PrintDefaults()
		os.Stdout.WriteString(`
help: shows this message

clone: clone a repository with notes

sync: exchange message traffic with a peer repository, "origin" by default.

compose: take a message thread update on stdin in RFC5322 format

cat: dump selected message threads in RFC5322 format

list: list the IDs of selected threads

getconfig: dump repository YAML configuration to standard output 

setconfig: set repository YAML configuration from standard input

tui: run a TUI to read and reply to selected threads (default mode)

serve: run a Shimmer HTTP server
`)
	default:
		croak("unknown command verb '" + command + "'; try 'shimmer help'.")
	}
}

// end
