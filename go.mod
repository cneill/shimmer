// -*- mode: Go;-*-
module gitlab.com/esr/shimmer

go 1.13

require (
	github.com/Showmax/go-fqdn v1.0.0 // indirect
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/bytesparadise/libasciidoc v0.7.0 // indirect
	github.com/gdamore/tcell/v2 v2.5.1 // indirect
	github.com/gomarkdown/markdown v0.0.0-20220603122033-8f3b341fef32 // indirect
	golang.org/x/term v0.0.0-20220411215600-e5f449aeb171 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
