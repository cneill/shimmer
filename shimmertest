#! /bin/sh
#
# Test suite for shimmer. Meant to be run after the tests
# of Git notes and the unit tessts for the Go code; this
# exercises the CLI.
#
# Reports in TAP
#
# Call with arguments to start a program in the trest-repository collectoion.
#
# For example, this tests the webserver - it will tell you where to point your browser.
#
# ./shimmertest serve
#
# This tests the TUI on the apple repository:
#
# ./shimmertest -c apple -- tui
#
# This dumps all messages in the apple repository as a mailbox file:
#
# ./shimmertest -c apple -- cat
#
# This simulates passing a query to the webserver, and returns a JSON
# reponse on stdout:
#
# ./shimmertest query 'action=project&repo=apple&format=json'

unset CDPATH	# See https://bosker.wordpress.com/2012/02/12/bash-scripters-beware-of-the-cdpath/

# Not all platforms have a 'realpath' command, so fake one up if needed
# using $PWD.
# Note: GitLab's CI environment does not seem to define $PWD, however, it
# does have 'realpath', so use of $PWD here does no harm.
command -v realpath >/dev/null 2>&1 ||
    realpath() { test -z "${1%%/*}" && echo "$1" || echo "$PWD/${1#./}"; }

subdir=""
while getopts c:- opt
do
    case $opt in
	c) subdir="$OPTARG";;
	-) break;;
	*) cat <<EOF
shimmertest - unit tests for the shimmer CLI

With -c, select a repository to cd into before running the closing command

Following args, if any, are passed to ../shimmer to invoke a shimmer
subcommand after all tests have been done but before the log dump.

When such a command is given, generation of TAP success messages and
the trailing test count is suppressed. Failure messages if any are
emitted to stderr; after a failure the script bails out.  Thus,
the command output can be captured by redirection.

Use -c to set a subdirectory for it to run in, if required.

Options for shimmer itself can be given before a "--", which tells this
shellscript to stop parsing the command line.

EOF
	   exit 0;;
    esac
done
# shellcheck disable=SC2004
shift $(($OPTIND - 1))

# shellcheck disable=2124
args=$@

if [ -n "$args" ]
then
    tapenable=no
    failredirect="1>&2"
    QUIET=1
else
    tapenable=yes
    failredirect=""
fi

here=$(realpath .)
trap 'rm -fr $here/apple $here/orange $here/pear /tmp/tapdiff$$' EXIT HUP INT QUIT TERM

# Set the PATH to include the current directory, so the repository
# head version of src can always be tested.
PATH="$(pwd)":$PATH

testcount=1
exitval=0

tapcd () {
    # If tapcd fails something is badlyu wrong with our test script. Bail out 
    cd "$1" >/dev/null || { echo "Bail out! $0: cd failed"; exit 1; }
}

check() {
    # shellcheck disable=SC2086
    case $? in
	0) [ "$tapenable" = no ] || echo "ok ${testcount} - $1";;
	*) echo "not ok ${testcount} - $1" ${failredirect}; exitval=1;;
    esac
    testcount=$((testcount + 1))
}

commit() {
    git commit -q -a -m "$1"
}

setuser() {
    git config user.name "$1"
    git config user.email "$2"
}

compose() {
    shimmer -T $testcount compose >seen$$
}

tapdiffer() {
    legend=$1
    checkfile=$2

    if diff --text -u "${checkfile}" - >/tmp/tapdiff$$
    then
	[ "$tapenable" = no ] || echo "ok $testcount - ${legend}"
	testcount=$((testcount + 1))
	return 0
    else
	# shellcheck disable=SC2086
	echo "not ok $testcount - ${legend}" ${failredirect}
	if [ ! "${QUIET}" = 1 ]
	then
	    echo "  --- |"
	    sed </tmp/tapdiff$$ -e 's/^/  /'
	    echo "  ..."
	fi
	testcount=$((testcount + 1))
	return 1
    fi
}

git init -q apple
check "create first test repo"

tapcd apple

git config shimmer.mta cat

setuser "Fred Foonly" fred@foonly.com

test "$(shimmer list 2>&1)" = "shimmer: can't operate on an empty repository"
check "list on empty repository complains"

test "$(shimmer cat 2>&1)" = "shimmer: can't operate on an empty repository"
check "cat on empty repository complains"

echo "Anchor file for apple notes" >README
git add README

commit "The comment doesn't matter"
check "anchor commit for apple"

test -z "$(shimmer list 2>&1)"
check "list is zero-length before notes"

test -z "$(shimmer cat 2>&1)"
check "cat is zero-length before notes"

compose <<EOF
Subject: Example note.

Should result in an ID being emitted.

EOF
cat >expect$$ <<EOF
issue:0001-01-01T00:00:07.000Z!fred@foonly.com
To: Fred Foonly <fred@foonly.com>
From: Fred Foonly <fred@foonly.com>
Date: Mon, 01 Jan 0001 00:00:07 +0000
Message-ID: <0001-01-01T00:00:07.000Z!fred@foonly.com>
Subject: Example note.
X-Message-Tags: issue:0001-01-01T00:00:07.000Z!fred@foonly.com
Content-Length: 39

Should result in an ID being emitted.

EOF
tapdiffer <seen$$ "Thread creation test" expect$$

shimmer setconfig "Shimmer configuration metadata" <<EOF
description: Apple is a small test repository
EOF
check "first configuration commit"

compose <<EOF
X-Message-Tags: issue:0001-01-01T00:00:07.000Z!fred@foonly.com

Thread followup
EOF
cat >expect$$ <<EOF
issue:0001-01-01T00:00:07.000Z!fred@foonly.com
To: Fred Foonly <fred@foonly.com>
From: Fred Foonly <fred@foonly.com>
Date: Mon, 01 Jan 0001 00:00:09 +0000
Message-ID: <0001-01-01T00:00:09.000Z!fred@foonly.com>
X-Message-Tags: issue:0001-01-01T00:00:07.000Z!fred@foonly.com
Content-Length: 16

Thread followup
EOF
tapdiffer <seen$$ "Thread followup" expect$$

setuser "Wilma Foonly" wilma@foonly.com

compose <<EOF
Subject: Second example thread.

Should result in a second distinct ID being omitted.
EOF
cat >expect$$ <<EOF
issue:0001-01-01T00:00:10.000Z!wilma@foonly.com
To: Wilma Foonly <wilma@foonly.com>
From: Wilma Foonly <wilma@foonly.com>
Date: Mon, 01 Jan 0001 00:00:10 +0000
Message-ID: <0001-01-01T00:00:10.000Z!wilma@foonly.com>
Subject: Second example thread.
X-Message-Tags: issue:0001-01-01T00:00:10.000Z!wilma@foonly.com
Content-Length: 53

Should result in a second distinct ID being omitted.
EOF
tapdiffer <seen$$ "Second thread" expect$$

# This commit is a test for line-wrapping in shimmer browse
compose <<EOF
Subject: Declaration of Independence
X-Message-Tags: declaration
X-Markup: asciidoc

In CONGRESS, July 4th 1776

The unanimous declaration of the thirteen states of America

*When*, in the course of human events, it becomes necessary for one people to dissolve the political bands which have connected them with another, and to assume, among the powers of the earth, the separate and equal station to which the laws of nature and of nature’s God entitle them, a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation.

We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable rights, that among these are life, liberty, and the pursuit of happiness. That, to secure these rights, governments are instituted among men, deriving their just powers from the consent of the governed. That, whenever any form of government becomes destructive of these ends, it is the right of the people to alter or to abolish it, and to institute new government, laying its foundation on such principles, and organizing its powers in such form, as to them shall seem most likely to effect their safety and happiness.

Prudence, indeed, will dictate that governments long established should not be changed for light and transient causes; and, accordingly, all experience has shown, that mankind are more disposed to suffer, while evils are sufferable, than to right themselves by abolishing the forms to which they are accustomed.

But, when a long train of abuses and usurpations, pursuing invariably the same object, evinces a design to reduce them under absolute despotism, it is their right, it is their duty, to throw off such government, and to provide new guards for their future security. Such has been the patient sufferance of these colonies; and such is now the necessity which constrains them to alter their former systems of government. The history of the present King of Great Britain is a history of repeated injuries and usurpations, all having in direct object the establishment of an absolute tyranny over these states. To prove this, let facts be submitted to a candid world.

He has refused his assent to laws the most wholesome and necessary for the public good.

He has forbidden his governors to pass laws of immediate and pressing importance, unless suspended in their operation till his assent should be obtained; and when so suspended, he has utterly neglected to attend to them.

He has refused to pass other laws for the accommodation of large districts of people, unless those people would relinquish the right of representation in the legislature; a right inestimable to them and formidable to tyrants only.

He has called together legislative bodies at places unusual, uncomfortable, and distant from the depository of their public records, for the sole purpose of fatiguing them into compliance with his measures.

He has dissolved representative houses repeatedly, for opposing, with manly firmness, his invasions on the rights of the people.

He has refused for a long time, after such dissolutions, to cause others to be elected; whereby the legislative powers, incapable of annihilation, have returned to the people at large for their exercise; the state remaining in the meantime exposed to all the dangers of invasion from without, and convulsions within.

He has endeavored to prevent the population of these states; for that purpose obstructing the laws for naturalization of foreigners; refusing to pass others to encourage their migrations hither, and raising the conditions of new appropriations of lands.

He has obstructed the administration of justice, by refusing his assent to laws for establishing judiciary powers.

He has made judges dependent on his will alone, for the tenure of their offices, and the amount and payment of their salaries.

He has erected a multitude of new offices, and sent hither swarms of officers to harass our people, and eat out their substance.

He has kept among us, in times of peace, standing armies, without the consent of our legislatures.

He has affected to render the military independent of and superior to the civil power.

He has combined with others to subject us to a jurisdiction foreign to our constitution, and unacknowledged by our laws; giving his assent to their acts of pretended legislation:

For quartering large bodies of armed troops among us;

For protecting them, by a mock trial, from punishment for any murders which they should commit on the inhabitants of these states;

For cutting off our trade with all parts of the world;

For imposing taxes on us without our consent;

For depriving us, in many cases, of the benefits of trial by jury;

For transporting us beyond seas to be tried for pretended offenses;

For abolishing the free system of English laws in a neighboring province, establishing therein an arbitrary government, and enlarging its boundaries, so as to render it at once an example and fit instrument for introducing the same absolute rule into these colonies;

For taking away our charters, abolishing our most valuable laws, and altering fundamentally the forms of our governments;

For suspending our own legislatures, and declaring themselves invested with power to legislate for us in all cases whatsoever.

He has abdicated government here, by declaring us out of his protection, and waging war against us.

He has plundered our seas, ravaged our coasts, burnt our towns, and destroyed the lives of our people.

He is at this time transporting large armies of foreign mercenaries to complete the works of death, desolation, and tyranny, already begun with circumstances of cruelty and perfidy scarcely paralleled in the most barbarous ages, and totally unworthy the head of a civilized nation.

He has constrained our fellow citizens, taken captive on the high seas, to bear arms against their country, to become the executioners of their friends and brethren, or to fall themselves by their hands.

He has excited domestic insurrections amongst us, and has endeavored to bring on the inhabitants of our frontiers, the merciless Indian savages, whose known rule of warfare is an undistinguished destruction of all ages, sexes, and conditions.

In every stage of these oppressions, we have petitioned for redress, in the most humble terms. Our repeated petitions have been answered only by repeated injury. A prince, whose character is thus marked by every act which may define a tyrant, is unfit to be the ruler of a free people.

Nor have we been wanting in attentions to our British brethren. We have warned them from time to time of attempts by their legislature to extend an unwarrantable jurisdiction over us. We have reminded them of the circumstances of our emigration and settlement here. We have appealed to their native justice and magnanimity, and we have conjured them by the ties of our common kindred, to disavow these usurpations, which would inevitably interrupt our connections and correspondence. They too have been deaf to the voice of justice and of consanguinity. We must, therefore, acquiesce in the necessity, which denounces our separation, and hold them, as we hold the rest of mankind, enemies in war, in peace friends.

We, therefore, the representatives of the United States of America, in General Congress assembled, appealing to the Supreme Judge of the world for the rectitude of our intentions, do, in the name, and by authority of the good people of these colonies, solemnly publish and declare, that these United Colonies are, and of right ought to be free and independent states; that they are absolved from all allegiance to the British Crown, and that all political connection between them and the state of Great Britain is and ought to be totally dissolved; and that, as free and independent states, they have full power to levy war, conclude peace, contract alliances, establish commerce, and to do all other acts and things which independent states may of right do. And for the support of this declaration, with a firm reliance on the protection of Divine Providence, we mutually pledge to each other our lives, our fortunes, and our sacred honor.
EOF

compose <<EOF
Subject: Feature-branch merge request
X-Merge-Request: featurebranch

Should result in a distinct ID being omitted, and a message
that is visible as an MR.
EOF

cat >expect$$ <<EOF
issue:0001-01-01T00:00:07.000Z!fred@foonly.com
issue:0001-01-01T00:00:10.000Z!wilma@foonly.com
declaration
merge-request:0001-01-01T00:00:11.000Z!wilma@foonly.com
EOF
shimmer list >seen$$
tapdiffer <seen$$ "ID list comparison" expect$$

setuser "Fred Foonly" fred@foonly.com

# This puts Fred on the subsciption list for Wilma's thread
compose <<EOF
X-Message-Tags: issue:0001-01-01T00:00:10.000Z!wilma@foonly.com

Our lives, our fortunes, and our sacred honor.
EOF

# This message has an attachment
compose <<EOF
From: Eric Raymond <esr@thyrsus.com>
Date: Sat, 09 Jul 2022 17:28:01 -0400
X-Message-Tags: issue:0001-01-01T00:00:07.000Z!fred@foonly.com
X-Markup: literal
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=----WebKitFormBoundary0AwFtg5Syoe5YLA9
Content-Length: 293

------WebKitFormBoundary0AwFtg5Syoe5YLA9
Content-Type: text/plain

This issue has an attachment.
------WebKitFormBoundary0AwFtg5Syoe5YLA9
Content-Disposition: attachment; filename=sample.txt
Content-Type: binary

For multipart experiment.

------WebKitFormBoundary0AwFtg5Syoe5YLA9--
EOF

# Thiis allows testing dequeue and discard
mkdir -p .git/shimmer/spool
cat >'.git/shimmer/spool/<2022-06-30T19:56:54.452Z!esr@thyrsus.com>' <<EOF
From: Eric Raymond <esr@thyrsus.com>
Date: Thu, 30 Jun 2022 15:56:54 -0400
Message-ID: <2022-06-30T19:56:54.452Z!esr@thyrsus.com>
X-Message-Tags: issue:0001-01-01T00:00:07.000Z!fred@foonly.com
X-Markup: literal

Foo.
Bar.
EOF

cd "../"
git init -q orange
check "create second test repo"

tapcd orange

git config shimmer.mta cat

setuser "Fred Foonly" fred@foonly.com

echo "Anchor file for orange notes" >README
git add README
commit "The comment doesn't matter"
check "anchor commit in orangw"

shimmer setconfig <<EOF
description: One commit, no Shimmer messages.
EOF
check "second configuration commit"

shimmer getconfig | grep "no Shimmer" >/dev/null
check "verify configuration write"

cd "../"

# Make this so we can test to see if remotes are visible
shimmer clone apple pear
check "repository clone of apple to pear succeeeded"

(cd pear >/dev/null || exit 1; shimmer cat | grep 'In CONGRESS, July 4th 1776' >/dev/null)
check "shimmer log was copied with clone"

(cd pear >/dev/null || exit 1; shimmer getconfig | grep "small test repository" >/dev/null)
check "shimmer configuration was copied with clone"

# More tests go here

#cd "../"
if [ -n "${subdir}" ]
then
    # shellcheck disable=SC2086
    cd ${subdir}* || exit 1
fi

if [ -n "$args" ]
then
    #echo "Running in $(pwd)"
    PATH="$PATH":..
    # shellcheck disable=SC2086
    shimmer $args
fi

if [ "$tapenable" = yes ]
then
    echo "1..$((testcount-1))"
fi

exit $exitval
